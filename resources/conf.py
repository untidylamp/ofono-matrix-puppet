import configparser
filepath="/home/phablet/.config/matrixtexting.ini"
parser=configparser.ConfigParser()

def reader(section, key):
  parser.read(filepath)
  return parser[section][key]

def writer(section, key, value):
  parser[section].update({key:value})
  write_changes()
  return 0

def write_changes():
  with open(filepath, "w") as file_obj:
    parser.write(file_obj)
  return 0
