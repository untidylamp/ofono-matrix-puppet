from resources.logging import log
import resources.conf as conf
import resources.database as db
import resources.contacts as contacts

#For login and to get token
from matrix_client.client import MatrixClient
# For the rest of the script.
from matrix_client.api import MatrixHttpApi

import time
import copy
from pathlib import Path
import urllib.request
import random

trusted_senders_list = conf.reader("matrix", "trusted_senders_csv").split(",")
bot_account = conf.reader("matrix", "bot")
sync_sleep = int(conf.reader("matrix", "sleep_between_sync"))

def login():
  log("info", "[matrix.py] [login] Starting")
  token = None

  try:
    token = conf.reader("matrix", "token")
  except:
    log("info", "[matrix.py] [login] No Token.")

  if token is None:
    try:
      client = MatrixClient(conf.reader("matrix", "url"))
      token = client.login(username=bot_account, password=conf.reader("matrix", "pass"))
    except Exception as problem:
      log("debug", "[matrix.py] [login] problem: " + str(problem))

    if token:
      log("debug", "[matrix.py] [login] Token:" + token)
      conf.writer("matrix", "token", token)
      conf.writer("matrix", "pass", "<using token>")
      first_sync()
    else:
      log("info", "[matrix.py] [login] Login failed.")


  log("info", "[matrix.py] [login] Done")
  return token

def first_sync():
  log("info", "[matrix.py] [first_sync] Starting.")
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))

  # Get the current sync_time and start bridging messages after now
  # this takes forever on old accounts - find a better way to do this?
  first_sync = matrix_api.sync(set_presence="online")
  first_sync_time = first_sync["next_batch"]
  log("debug", "[matrix.py] [first_sync] Discarding all events up to: " + first_sync_time)

  db.update_matrix_sync(first_sync_time)
  #conf.writer("matrix", "next_batch", first_sync_time)

  log("info", "[matrix.py] [first_sync] Done")
  return first_sync_time

def get_topic(room):
  log("info", "[matrix.py] [get_topic] Starting.")
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))

  topic = None
  try:
    dict_topic = matrix_api._send("GET", "/rooms/" + room + "/state/m.room.topic")
    topic = str(dict_topic['topic'])
    log("debug", "[matrix.py] [get_topic] topic: " + topic)
  except Exception as problem:
    log("error", "[matrix.py] [get_topic] Failed to get Topic.")
    log("debug", "[matrix.py] [get_topic] error: " + str(problem))

  log("info", "[matrix.py] [get_topic] Done.")
  return topic

def new_invite(status_queue, room, content):
  log("info", "[matrix.py] [new_invite] Starting.")
  log("debug", "[matrix.py] [new_invite] room: " + str(room))
  log("debug", "[matrix.py] [new_invite] content: " + str(content))

  sender = str(content['invite_state']['events'][0]['sender'])

  if sender is bot_account:
    log("info", "[matrix.py] [new_invite] ignoring bot account.")
    return 0
  elif sender not in trusted_senders_list:
    log("warning", "[matrix.py] [new_invite] Ignoring Untrusted Sender. Check debug logs for username and update in config.ini to trust.")
    log("info", "[matrix.py] [new_invite] Ignoring from: " + sender)
    return 0

  log("info", "[matrix.py] [new_invite] Trusted sender.")

  encrypted = False
  for event in content['invite_state']['events']:
    if event['type'] == 'm.room.encryption':
      log("warning","[matrix.py] [new_invite] invited to an encrypted room.")
      encrypted = True

  matrix_status = {
    "matrix_room": room,
    "state": "invite",
    "encrypted": encrypted,
  }

  status_queue.put(copy.deepcopy(matrix_status))
  log("info", "[matrix.py] [new_invite] Done.")
  return 0

def new_event(status_queue, message_queue, room, event):
  log("info", "[matrix.py] [new_event] Starting.")
  log("debug", "[matrix.py] [new_event] room: " + str(room))
  log("debug", "[matrix.py] [new_event] content: " + str(event))

  sender = str(event['sender'])
  room = str(room)

  if sender is bot_account:
    log("info", "[matrix.py] [new_event] ignoring bot account.")
    return 0

  elif sender not in trusted_senders_list:
    log("warning", "[matrix.py] [new_event] Ignoring Untrusted Sender. Check debug logs for username and update in config.ini to trust.")
    log("info", "[matrix.py] [new_event] Ignoring from: " + sender)
    return 0

  if event['type'] == 'm.room.message':
    log("info", "[matrix.py] [new_event] New message in room.")
    matrix_message = {"id": 0,                                      # row id in the message database.
                      "rooms_id": 0,                                # row id in the rooms database (what room it's in)
                      "matrix_room": room,                     # Room in the remote matrix server
                      "sender": sender,                             # who sent the m.room.message
                      "recipients": [],                             # who the content is going to (get from DB later, don't trust topic)
                      "send_via_modem": True,                       # send via modem ( Matrix -> python app -> modem -> destination)
                      "sent_time": str(event['origin_server_ts']),  # time it was sent
                      "content_type": "",                           # mime type (text/plain, image/jpeg, etc.)
                      "content": "",                                # Content or [http|mxc] url
                      "subject": "",                                # empty (m.text) filename (m.file)
                      "content_size": "",                           # size (bytes) of the content we have received.
                      "bridged": False,                             # has the message made it to it's destination
                      "locked": True,                               # is work in progress for this message
                      "attempt": 0,                                 # the number of attempts to bridge the message
                      "matrix_event_id": str(event['event_id']),    # Event ID from matrix
                      "modem_event_id": "",                         # Event ID from the modem
                      }
    if 'msgtype' in event['content'].keys() and event['content']['msgtype'] == 'm.text':
      log("info", "[matrix.py] [new_event] plain text.")
      matrix_message['content_type'] = "text/plain"
      matrix_message['content'] = str(event['content']['body'])
      matrix_message['content_size'] = len(str(event['content']['body']))
    elif 'url' in event['content'].keys():
      log("info", "[matrix.py] [new_event] file. Trying to convert mxc url to a URL we can use.")
      url = mxc_to_url(event['content']['url'])
      if url.startswith("http"):
        log("info", "[matrix.py] [new_event] converted mcx to url and using that")
        matrix_message['content'] = str(url)
      else:
        log("warning", "[matrix.py] [new_event] can't convert mxc to url so we'll keep it to avoid data loss.")
        matrix_message['content'] = str(event['content']['url'])

      if 'mimetype' in event['content']['info'].keys():
        matrix_message['content_type'] = str(event['content']['info']['mimetype'])
      else:
        log("warning", "[matrix.py] [new_event] mimetype is missing from the message metadata")
        matrix_message['content_type'] = "mimetype/missing"

      if 'size' in event['content']['info'].keys():
        matrix_message['content_size'] = int(event['content']['info']['size'])
      else:
        log("warning", "[matrix.py] [new_event] file size is missing from the message metadata")
        matrix_message['content_size'] = 0

      matrix_message['subject'] = str(event['content']['body'])
    else:
      log("info", "[matrix.py] [new_event] message not supported.")
      return 0

    log("debug", "[matrix.py] [new_event] posting to message queue " + str(matrix_message))
    message_queue.put(copy.deepcopy(matrix_message))

  elif event['type'] == 'm.room.name':
    log("info", "[matrix.py] [new_event] room name change.")
    try:
      new_name = str(event['content']['name'])
      log("debug", "[matrix.py] [new_event] new name: " +new_name)
    except Exception as problem:
      new_name = None
      log("info", "[matrix.py] [new_event] Matrix room name change likely redacted.")
      log("debug", "[matrix.py] [new_event] Failed to get new name: " +str(problem))

    if new_name:
      matrix_status = {
        "matrix_room": room,
        "sender": sender,
        "state": "room_name",
        "name": new_name
      }
      log("debug", "[matrix.py] [new_event] Putting on the status_queue: " + str(matrix_status))
      status_queue.put(copy.deepcopy(matrix_status))

  elif event['type'] == 'm.room.member':
    log("info", "[matrix.py] [new_event] m.room.member.")
    if event['content']['membership'] == 'leave':
      matrix_status = {
        "matrix_room": room,
        "sender": sender,
        "state": "leave",
      }
      log("debug", "[matrix.py] [new_event] Putting on the status_queue: " + str(matrix_status))
      status_queue.put(copy.deepcopy(matrix_status))

  elif event['type'] == 'm.room.encrypted':
    log("warning", "[matrix.py] [new_event] Encrypted message.")
    send_emote(room, "does not support encrypted messages.")

  else:
    log("debug", "[matrix.py] [new_event] ignoring event type: " + str(event['type']))

  log("info", "[matrix.py] [new_event] Done.")

def sync(stop_event, outgoing_message_queue, message_status_queue):
  log("info", "[matrix.py] [sync] Starting.")
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))


  while not stop_event.is_set():
    sync_since = db.get_matrix_sync()
    log("debug", "[matrix.py] [sync] start sync time: " + sync_since)

    try:
      log("info", "[matrix.py] [sync] waiting for new information.")
      sync_info = matrix_api.sync(since=sync_since, set_presence="online", timeout_ms=conf.reader("matrix", "sync_timeout_ms"))
      db.update_matrix_sync(sync_info["next_batch"])
    except Exception as problem:
      log("error", "[matrix.py] [sync] failed to sync. sleeping.")
      log("debug", "[matrix.py] [sync] problem: "+ str(problem))
      time.sleep(sync_sleep)
      continue


    log("info", "[matrix.py] [sync] new sync data.")
    log("debug", "[matrix.py] [sync] sync data: " + str(sync_info))
    # Deal with the sync events from Matrix.
    if "rooms" in sync_info:
      for type, content in sync_info['rooms'].items():

        if type == "invite":
          for room in content:
            log("debug", "[matrix.py] [sync] invite for room: " + str(room))
            new_invite(message_status_queue, room, content[room])

        elif type == "join":
          log("debug", "[matrix.py] [sync] join content: " + str(content))
          for room, room_content in sync_info['rooms']['join'].items():
            log("debug", "[matrix.py] [sync] room: " + str(room))
            log("debug", "[matrix.py] [sync] room content: " + str(room_content))

            # timeline events are where the messages live. I only care about them.
            # ephemeral events hold stuff like m.typing.
            for event in room_content['timeline']['events']:
              log("debug", "[matrix.py] [sync] event: " + str(event))
              new_event(message_status_queue, outgoing_message_queue, room, event)

        else:
          log("debug", "[matrix.py] [sync] ignoring event type: " + str(type))

    log("info", "[matrix.py] [sync] Done with new data. Sleeping.")
    time.sleep(sync_sleep)
  log("info", "[matrix.py] [sync] Done.")
  return 0

def build_room(participants=None):
  if participants is None:
    participants = []
  log("info", "[matrix.py] [build_room] Starting")
  matrix_room = None
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))

  if len(participants) == 1:
    log("info", "[matrix.py] [build_room] one to one message")
    is_direct_chat = True
  else:
    log("info", "[matrix.py] [build_room] Group Chat")
    is_direct_chat = False


  # convert numbers to names for the name of the room.
  name = ', '.join(contacts.get_name(number) for number in participants)
  topic = ', '.join(participants)


  try:
    json_body = { "is_direct": is_direct_chat,"creation_content": {"m.federate": "false" }, "preset": "private_chat", "invite": trusted_senders_list, "topic": topic, "name": name}
    room = matrix_api._send("POST", "/createRoom", json_body)
    matrix_room = room['room_id']
    log("debug", "[matrix.py] [build_room] Room created: " + str(matrix_room))
  except Exception as problem:
    log("info", "[matrix.py] [build_room] Failed to build room.")
    log("debug", "[matrix.py] [build_room] Problem: " + str(problem))

  log("info", "[matrix.py] [build_room] Done")
  return matrix_room

def send_text(room, message):
  log("info", "[matrix.py] [send_text] Starting")
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))
  message['matrix_event_id'] = None


  if len(message['recipients']) > 1:
    log("info", "[matrix.py] [send_text] More than one recipient (group message) adding name to message.")
    sender = contacts.get_name(message['sender'])
    content = sender + ": " + message['content']
  else:
    content = message['content']

  puppet_pack_info = {
    "db_message_id": message["id"],
    "db_rooms_id": message["rooms_id"],
    "sender": message["sender"],
    "recipients": ','.join(message["recipients"]),
    "attempt": message["attempt"],
    "sent_time": message["sent_time"],
    "content_type": message["content_type"],
    "content_size": message["content_size"],
    "subject": message["subject"],
    "modem_event_id": message["modem_event_id"],
  }

  content_pack = {
    "msgtype": "m.text",
    "body": content,
    "MatrixTexting": puppet_pack_info,
  }

  try:
    matrix = matrix_api.send_message_event(room_id=room,event_type="m.room.message",content=content_pack)
    message['matrix_event_id'] = str(matrix['event_id'])
    log("info", "[matrix.py] [send_text] message bridged to matrix.")
    log("debug", "[matrix.py] [send_text] matrix event id: " + message['matrix_event_id'])
  except Exception as problem:
    log("error", "[matrix.py] [send_text] Failed to bridge message.")
    log("debug", "[matrix.py] [send_text] error: " + str(problem))


  log("info", "[matrix.py] [send_text] Done")
  return message['matrix_event_id']

def send_emote(room, message):
  log("info", "[matrix.py] [send_emote] Starting")
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))

  content_pack = {
    "msgtype": "m.emote",
    "body": message,
  }

  matrix_event_id = None
  try:
    matrix_event_id = matrix_api.send_message_event(room_id=room,event_type="m.room.message",content=content_pack)
    log("debug", "[matrix.py] [send_emote] Message sent: " + str(matrix_event_id))
  except Exception as problem:
    log("error", "[matrix.py] [send_emote] Failed to send message to room.")
    log("debug", "[matrix.py] [send_emote] error: " + str(problem))

  log("info", "[matrix.py] [send_emote] Done")
  return matrix_event_id

def send_file(room, message):
  log("info", "[matrix.py] [send_file] Starting")
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))
  matrix_event_id = None

  #read in bites to send to matrix.
  try:
    file = open(message['content'], "rb")
    attachment  = file.read()
    file.close()
    log("debug", "[matrix.py] [send_file] read File: " + str(message['content']))
  except Exception as problem:
    log("error", "[matrix.py] [send_file] Failed to read File.")
    log("debug", "[matrix.py] [send_file] error: " +str(problem))
    return None

  #upload file and get mxc link.
  try:
    mxc_url=matrix_api.media_upload(content_type=message["content_type"], content=attachment)
    log("debug", "[matrix.py] [send_file] mxc_url: " + str(mxc_url))
  except Exception as problem:
    log("error", "[matrix.py] [send_file] Failed to upload File to matrix.")
    log("debug", "[matrix.py] [send_file] error uploading: " +str(problem))
    return None

  if not mxc_url['content_uri']:
    log("error", "[matrix.py] [send_file] unexpected reply for mxc_url ")
    return None

  message['content'] = mxc_to_url(mxc_url['content_uri'])
  log("debug", "[matrix.py] [send_file] http URL of file on server: " + message['content'])

  if "image" in message["content_type"]:
    msgtype = "m.image"
  else:
    msgtype = "m.file"

  puppet_pack_info = {
    "db_message_id": message["id"],
    "db_rooms_id": message["rooms_id"],
    "sender": message["sender"],
    "recipients": ','.join(message["recipients"]),
    "attempt": message["attempt"],
    "sent_time": message["sent_time"],
    "http_url": message['content'],
    "content_type": message["content_type"],
    "content_size": message["content_size"],
    "subject": message["subject"],
    "modem_event_id": message["modem_event_id"],
  }
  content_pack_info = {
    "mimetype": message["content_type"],
  }

  sender = contacts.get_name(message['sender'])

  content_pack = {
    "msgtype": msgtype,
    "body": message["content_type"] + " from " + sender,
    "url": mxc_url['content_uri'],
    "MatrixTexting": puppet_pack_info,
    "info": content_pack_info,
  }


  #send the mxc link to matrix as a m.room.message with a type of m.image.
  try:
    matrix_id = matrix_api.send_message_event(room_id=room,event_type="m.room.message",content=content_pack)
    log("info", "[matrix.py] [send_file] mms uploaded to matrix room.")
    log("debug", "[matrix.py] [send_file] file: " + str(message["content"]) + "  room: " + str(room) + " ID: " + str(matrix_id))

  except Exception as problem:
    log("info", "[matrix.py] [send_file] we have mxc url but failed to post to room. ")
    log("debug", "[matrix.py] [send_file] error: " + str(problem))
    return None

  if not matrix_id['event_id']:
    log("debug", "[matrix.py] [send_file] Did not get the expected reply.")
    log("debug", "[matrix.py] [send_file] event ID is weird: " + str(matrix_id))
    return None

  message['matrix_event_id'] = matrix_id['event_id']

  log("info", "[matrix.py] [send_file] Done")
  return message['matrix_event_id']

def mxc_to_url(mxc):
  log("info", "[matrix.py] [mxc_to_url] Starting")
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))

  try:
    url=matrix_api.get_download_url(mxc)
    log("debug", "[matrix.py] [mxc_to_url] mxc converted to: " + str(url))
  except Exception as problem:
    log("error", "[matrix.py] [mxc_to_url] cant convert mxc to url.")
    log("debug", "[matrix.py] [mxc_to_url] error: " + str(problem))
    url=mxc

  log("info", "[matrix.py] [mxc_to_url] Done")
  return url

def join_room(room):
  log("info", "[matrix.py] [join_room] Starting")
  log("debug", "[matrix.py] [join_room] room: " + str(room))
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))

  joined = False

  path=str("/join/"+room)
  method="POST"
  try:
    matrix_api._send(method=method, path=path)
    joined = True
    log("info", "[matrix.py] [join_room] Joined room.")

  except Exception as problem:
    log("error", "[matrix.py] [join_room] Problem Joining room.")
    log("debug", "[matrix.py] [join_room] Problem: " + str(problem))

  log("info", "[matrix.py] [join_room] Done")
  return joined

def read_receipt(room_id, event_id):
  log("info", "[matrix.py] [read_receipt] Started")
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))

  read_receipt = None
  json_body = { 'm.fully_read': event_id, 'm.read': event_id}
  try:
    read_receipt = matrix_api._send("POST", "/rooms/"+room_id+"/read_markers", json_body)
    log("info", "[matrix.py] [read_receipt] moved read marker.")
  except Exception as problem:
    log("error", "[matrix.py] [read_receipt] problem moving read marker.")
    log("debug", "[matrix.py] [read_receipt] error: " +str(problem))

  log("info", "[matrix.py] [read_receipt] Done")
  return read_receipt

def download_file(url):
  log("info", "[matrix.py] [download_file] Starting")

  try:
    tmp_dir = conf.reader("matrixtexting","tmp_outbound_attachment_dir")
    Path(tmp_dir).mkdir(parents=True, exist_ok=True)
    local_file = tmp_dir + str(int(time.time())) + "-" + str(random.randrange(10, 99)) + "_dl.mms"
    urllib.request.urlretrieve(url, local_file)
    log("debug", "[matrix.py] [download_file] Downloaded file to: " + local_file)
  except Exception as problem:
    log("info", "[matrix.py] [download_file] Download failed.")
    log("debug", "[matrix.py] [download_file] Downloaded error: " + str(problem))
    local_file = False

  log("info", "[matrix.py] [download_file] Done")
  return local_file

def set_display_name(room_id, display_name):
  log("info", "[matrix.py] [set_display_name] Starting")
  log("debug", "[matrix.py] [set_display_name] room_id " + str(room_id) + "  display_name " + str(display_name))
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))

  changed = False
  json_body = { 'membership': 'join', 'displayname': display_name}
  try:
    matrix_api._send("PUT", "/rooms/"+room_id+"/state/m.room.member/"+bot_account, json_body)
    changed = True
  except Exception as problem:
    log("info", "[matrix.py] [set_display_name] Failed to set room name.")
    log("debug", "[matrix.py] [set_display_name] error: " +str(problem))

  log("info", "[matrix.py] [set_display_name] Done")
  return changed

def change_permissions_in_room(room_id, user=""):
  log("info", "[matrix.py] [change_permissions_in_room] Starting")
  log("debug", "[matrix.py] [change_permissions_in_room] room: " + room_id + " user: " + user)
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))
  return_event = None
  json_body = {"users":{
                       bot_account:100,
                       #user:99
                       },
               "users_default":99,
               "events":{
                        "m.room.name":50,
                        "m.room.topic":100,
                        "m.room.power_levels":100,
                        "m.room.history_visibility":99,
                        "m.room.canonical_alias":50,
                        "m.room.avatar":50,
                        "m.room.aliases":50,
                        "m.room.tombstone":99,
                        "m.room.server_acl":99
                        },
               "events_default":50,
               "state_default":50,
               "ban":50,
               "kick":50,
               "redact":50,
               "invite":99
               }

  try:
    return_event = matrix_api._send("PUT", "/rooms/"+room_id+"/state/m.room.power_levels/", json_body)
  except Exception as problem:
    log("info", "[matrix.py] [change_permissions_in_room] Problem changing permissions.")
    log("debug", "[matrix.py] [change_permissions_in_room] error: " + str(problem))


  log("info", "[matrix.py] [change_permissions_in_room] Done")
  return return_event

def leave_room(room):
  log("info", "[matrix.py] [leave_room] Starting")
  log("debug", "[matrix.py] [leave_room] room: " + str(room))
  matrix_api = MatrixHttpApi(conf.reader("matrix", "url"), token=conf.reader("matrix", "token"))

  left = False

  try:
    matrix_api._send("POST", "/rooms/"+room+"/leave")
    left = True
    log("info", "[matrix.py] [leave_room] left room.")

  except Exception as problem:
    log("error", "[matrix.py] [leave_room] Problem Joining room.")
    log("debug", "[matrix.py] [leave_room] Problem: " + str(problem))

  log("info", "[matrix.py] [leave_room] Done")
  return left
