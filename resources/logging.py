import resources.conf as conf

import logging
from logging.handlers import RotatingFileHandler
import threading
from pathlib import Path

log_dir = conf.reader("log", "dir")
Path(log_dir).mkdir(parents=True, exist_ok=True)

log_formatter = logging.Formatter('[%(asctime)s] [%(levelname)s] %(message)s')
log_handler = RotatingFileHandler(log_dir+"/matrixtexting.log", maxBytes=int(conf.reader("log", "bytes")), backupCount=int(conf.reader("log", "history")))
log_handler.setFormatter(log_formatter)

logger = logging.getLogger()
level = conf.reader("log", "level")
if level == "debug":
  logger.setLevel(logging.DEBUG)
elif level == "warning":
  logger.setLevel(logging.WARNING)
elif level == "error":
  logger.setLevel(logging.ERROR)
elif level == "critical":
  logger.setLevel(logging.CRITICAL)
else:
  logger.setLevel(logging.INFO)

logger.addHandler(log_handler)

lock_log = threading.Lock()


# If you're printing a variable, use debug.
# generic app status/progress use info.
# warning/error/critical do not print a var. Give information in a string and another log line with debug for var if required.

def log(log_level, message):
  with lock_log:
    if log_level == 'info':       # Do NOT print variables. Just chatty information from dev about what the script is doing.
      logging.info(message)
    elif log_level == 'warning':  # Do NOT print variables. warning messages from dev. not a big deal. Turn on debug for more information.
      logging.warning(message)
    elif log_level == 'error':    # Do NOT print variables. caught unexpected thing happened. Turn on debug for more information.
      logging.error(message)
    elif log_level == 'critical': # Do NOT print variables. caught critical issue, app stopped. Turn on debug for more information.
      logging.critical(message)
    else:
      logging.debug(message)      # will have passwords, tokens, phone numbers, SMS data, MMS attachment data (large pictures WILL fill up logs), etc, etc, etc. Very VERY chatty.
