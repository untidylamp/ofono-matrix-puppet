from resources.logging import log
import resources.conf as conf
import phonenumbers


def get_name(number):
  log("info", "[contacts.py] [get_name] Starting")

  try:
    contacts_vcf = conf.reader("matrixtexting","contacts_vcf")
    file = open(contacts_vcf, 'r')
  except Exception as problem:
    log("warning", "[contacts.py] [get_name] Problem with contacts file: " + str(problem))
    return number

  number = format_number(number)
  name = None
  matched_number = False

  for line in file:
    #if we get to new information, clear the info we're saving
    if(line.startswith("BEGIN")):
      name=None
      matched_number=False

    #Grab the name for this vcard
    if line.startswith("FN:"):
      name=line.split(":",1)[1].rstrip()

    #Grab the tel number (can be more then one per contact)
    if line.startswith("TEL"):
      number_to_test=line.split(":",1)[1]
      number_to_test=format_number(number_to_test)
      #if the number matches, set matched to true.
      if number_to_test == number:
        matched_number=True

    #if we have a name and the number was matched at some point in this
    #vcard, we return the name.
    if name is not None and matched_number:
      file.close()
      return name

  #no match, return exactly was was passed to us.
  file.close()
  return number


def format_number(number):
  log("info", "[contacts.py] [format_number] Starting")
  log("debug", "[contacts.py] [format_number] Number: " + number)

  if is_number(number):
    log("info", "[contacts.py] [format_number] Is a number. trying to format.")
    try:
      formatted = phonenumbers.parse(number, conf.reader("cell", "country_code"))
      number = phonenumbers.format_number(formatted, phonenumbers.PhoneNumberFormat.E164)
    except Exception as problem:
      log("info", "[contacts.py] [format_number] Exception. see debug for problem.")
      log("debug", "[contacts.py] [format_number] ignoring error. " + str(problem))
  else:
    log("info", "[contacts.py] [format_number] Not a number. Return string unchanged.")
    # I guess some modems use fake "numbers" to talk to it. This is to support that.

  log("info", "[contacts.py] [format_number] new number: " +str(number))
  log("info", "[contacts.py] [format_number] Done")
  return str(number)


def is_number(number):
  log("info", "[contacts.py] [is_number] Starting")
  is_a_number = False
  try:
    formatted = phonenumbers.parse(number, conf.reader("cell","country_code"))
    is_a_number = phonenumbers.is_possible_number(formatted)
  except Exception as problem:
    log("info", "[contacts.py] [is_number] Exception. see debug for problem.")
    log("debug", "[contacts.py] [is_number] ignoring error. " + str(problem))

  log("debug", "[contacts.py] [is_number] number: " + str(number) + " Is a number: " + str(is_a_number))
  log("info", "[contacts.py] [is_number] Done")
  return is_a_number