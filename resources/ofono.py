from resources.logging import log
import resources.conf as conf
import resources.contacts as contacts
from gi.repository import GLib
import dbus
import dbus.mainloop.glib
import sys
from pathlib import Path
import time
import random
import magic
from pathlib import Path

import copy



attachments_dir = conf.reader("cell", "attachments_dir")
my_number = contacts.format_number(conf.reader("cell","number"))

def write_attachment_file(content, content_type, sending_number):
  log("info", "[ofono.py] [write_attachment_file] Starting")
  Path(attachments_dir).mkdir(parents=True, exist_ok=True)

  epoch=str(int(time.time()))
  random_number=str(random.randrange(10, 99))
  file_extension=content_type.split("/",1)[1]

  attachment_file=attachments_dir+"/"+sending_number+"_"+epoch+"-"+random_number+"."+file_extension
  log("debug", "[ofono.py] [write_attachment_file] attachment file: " + attachment_file)

  with open(attachment_file, 'wb') as f: 
    f.write(content)

  return attachment_file

def send_sms_message(phone_number, message):
  log("info", "[ofono.py] [send_sms_message] Starting")
  log("debug", "[ofono.py] [send_sms_message] Number: " + phone_number)
  log("debug", "[ofono.py] [send_sms_message] Message: " + message)

  try:
    bus = dbus.SystemBus()
    manager = dbus.Interface(bus.get_object('org.ofono', '/'),
                             'org.ofono.Manager')
    modems = manager.GetModems()
    path = modems[0][0]
    mm = dbus.Interface(bus.get_object('org.ofono', path),
                        'org.ofono.MessageManager')

    mm.SetProperty("UseDeliveryReports", dbus.Boolean(1))

    event_id = mm.SendMessage(phone_number, message)

    log("debug", "[ofono.py] [send_sms_message] event_id: " + event_id)
  except Exception as problem:
    log("info", "[ofono.py] [send_sms_message] Failed to send SMS.")
    log("debug", "[ofono.py] [send_sms_message] SMS error: " + str(problem))
    event_id = False


  log("info", "[ofono.py] [send_sms_message] Done")
  return event_id

def send_mms_message(phone_numbers=[], text="", mms_file_path="", mms_file_mime=""):
  log("info", "[ofono.py] [send_mms_message] Starting")
  log("debug", "[ofono.py] [send_mms_message] Number(s): " + str(phone_numbers))
  log("debug", "[ofono.py] [send_mms_message] text: " + text)
  log("debug", "[ofono.py] [send_mms_message] mms_file_path: " + mms_file_path)
  log("debug", "[ofono.py] [send_mms_message] mms_file_mime: " + mms_file_mime)

  #Build a DBUS Array that we're going to add our attachments to.
  attachments = dbus.Array([],signature=dbus.Signature('(sss)'))


  if text:
    log("info", "[ofono.py] [send_mms_message] Text needs to be in a file for an MMS message.")

    # Make sure dir exists and get a unique filename to use user that dir.
    outbound_dir=str(conf.reader("matrixtexting","tmp_outbound_attachment_dir"))
    Path(outbound_dir).mkdir(parents=True, exist_ok=True)
    text_mms_file=outbound_dir+str(int(time.time()))+"-"+str(random.randrange(10, 99))+".mms"
    log("debug", "[ofono.py] [send_mms_message] Text in file: " + str(text_mms_file))

    # Write our text to that .mms file
    with open(text_mms_file, 'w') as f: 
      f.write(text)

    # Add the text file attachment to our dbus array
    attachments.append(dbus.Struct((dbus.String("text.txt"),dbus.String("text/plain"),dbus.String(text_mms_file)),signature=None))


  if mms_file_path:
    log("info", "[ofono.py] [send_mms_message] File to attach.")
    if not mms_file_mime:
      log("info", "[ofono.py] [send_mms_message] Have to figure out mime ourself.")
      mms_file_mime=magic.from_file(mms_file_path, mime=True)
      log("debug", "[ofono.py] [send_mms_message] I think the file is mime type: " + str(mms_file_mime))

    # Add the attachment to our dbus array
    attachments.append(dbus.Struct((dbus.String("file"+mms_file_mime.split("/",1)[1]),dbus.String(mms_file_mime),dbus.String(mms_file_path)),signature=None))

  mms_modem_path = False
  if text or mms_file_path:
    log("debug", "[ofono.py] [send_mms_message] attachments for dbus: " + str(attachments))

    try:
      bus = dbus.SessionBus()
      log("debug", "[ofono.py] [send_mms_message] bus done" + str(bus))

      services = bus.call_blocking("org.ofono.mms", "/org/ofono/mms",
                                   "org.ofono.mms.Manager",
                                   "GetServices",
                                   None, [], timeout=5)
      log("debug", "[ofono.py] [send_mms_message] services done" + str(services))

      path = services[0][0]
      log("debug", "[ofono.py] [send_mms_message] path done" + str(path))

      mms_modem_path = bus.call_blocking("org.ofono.mms", path,
                               "org.ofono.mms.Service",
                               'SendMessage',
                                None, (phone_numbers, attachments), timeout=5)
      log("debug", "[ofono.py] [send_mms_message] mms_modem_path done" + str(mms_modem_path))
    except Exception as problem:
      log("info", "[ofono.py] [send_mms_message] Problem sending MMS message.")
      log("debug", "[ofono.py] [send_mms_message] MMS Error: " + str(problem))
  else:
    log("error", "[ofono.py] [send_mms_message] called without any attachments.... We should never get here.")

  # Clean up the .mms file we made with the text in it.
  if text:
    try: Path(text_mms_file).unlink() # missing_ok=True added in python 3.8
    except: pass

  log("info", "[ofono.py] [send_mms_message] Done")
  return mms_modem_path

def sms_listener(incoming_queue, status_queue):
  log("info", "[ofono.py] [sms_listener] Starting")
  # https://kernel.googlesource.com/pub/scm/network/ofono/ofono/+/refs/heads/master/test/receive-sms
  dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
  bus = dbus.SystemBus()


  def sms_incoming_message(message, details, path, interface):
    log("info", "[ofono.py] [sms_incoming_message] Starting")
    log("debug", "[ofono.py] [sms_incoming_message] message: "+str(message))
    log("debug", "[ofono.py] [sms_incoming_message] details: "+str(details))
    log("debug", "[ofono.py] [sms_incoming_message] path: "+str(path))
    log("debug", "[ofono.py] [sms_incoming_message] interface: "+str(interface))

    sms_message = {"id": 0,                                  # row id in the message database.
                   "rooms_id": 0,                            # row id in the rooms database (what room it's in)
                   "matrix_room": "",                        # Room in the remote matrix server
                   "sender": contacts.format_number(details['Sender']),  # who sent the file to me
                   "recipients": [my_number], # LIST of who got this message.
                   "send_via_modem": False,                  # send via modem ( Matrix -> python app -> modem -> destination)
                   "sent_time": str(details['SentTime']),    # time it was sent
                   "content_type": "text/plain",             # mime type (text/plain, image/jpeg, etc)
                   "content": str(message),                  # Content of the attahment in byte form
                   "subject": "",                            # Subject of the MMS
                   "content_size": len(str(message)),        # size (bytes) of the contect we have recieved.
                   "bridged": False,                         # has the message made it to matrix
                   "locked": True,                           # work in progress for this message (so we dont retry an in progress message when we check for failed ones)
                   "attempt": 0,                             # the number of attempts to bridge the message (so we can give up after X tries)
                   "matrix_event_id": "",                    # Event ID from matrix
                   "modem_event_id": str(path),              # Event ID from the modem
                 }

    ignore_list = conf.reader("cell", "ignore_csv").split(",")
    if sms_message['sender'] in ignore_list:
      log("info", "[ofono.py] [sms_incoming_message] Sender in ignore list")
      return 0

    log("debug", "[ofono.py] [sms_incoming_message] posting to message queue " + str(sms_message))
    incoming_queue.put(copy.deepcopy(sms_message))
    log("info", "[ofono.py] [sms_incoming_message] Done")

  def sms_outgoing_message_added(path, value):
    log("info", "[ofono.py] [sms_outgoing_message_added] Starting")
    log("debug", "[ofono.py] [sms_outgoing_message_added] path: "+str(path))
    log("debug", "[ofono.py] [sms_outgoing_message_added] value: "+str(value))

    sms_status = {"modem_event_id": str(path),
                  "state" : str(value['State']), 
                 }
    log("debug", "[ofono.py] [sms_outgoing_message_added] Putting on status_queue "+ str(sms_status))
    status_queue.put(copy.deepcopy(sms_status))
    log("info", "[ofono.py] [sms_outgoing_message_added] Done")


  def sms_property_changed(something, state, path, interface):
    if ( interface == "org.ofono.Message"): # TODO - yeah, I dont know how else to do this.
      log("info", "[ofono.py] [sms_property_changed] Starting")
      log("debug", "[ofono.py] [sms_property_changed] something: " +str(something))
      log("debug", "[ofono.py] [sms_property_changed] state: " +str(state))
      log("debug", "[ofono.py] [sms_property_changed] path: " +str(path))
      log("debug", "[ofono.py] [sms_property_changed] interface: " +str(interface))

      sms_status = {"modem_event_id": str(path),
                    "state" : str(state).lower(),
                   }
      log("debug", "[ofono.py] [sms_property_changed] Putting on status_queue "+ str(sms_status))
      status_queue.put(copy.deepcopy(sms_status))
      log("info", "[ofono.py] [sms_property_changed] Done")

  def sms_outgoing_message_removed(path):
    log("info", "[ofono.py] [sms_outgoing_message_removed] Starting")
    log("debug", "[ofono.py] [sms_property_changed] path: " +str(path))
    sms_status = {"modem_event_id": str(path),
                  "state" : "removed",
                 }
    log("debug", "[ofono.py] [sms_outgoing_message_removed] Puttong on status_queue "+ str(sms_status))
    status_queue.put(copy.deepcopy(sms_status))
    log("info", "[ofono.py] [sms_outgoing_message_removed] Done")

  bus.add_signal_receiver(sms_incoming_message,
                          bus_name="org.ofono",
                          signal_name = "ImmediateMessage",
                          path_keyword="path",
                          interface_keyword="interface")

  bus.add_signal_receiver(sms_incoming_message,
                          bus_name="org.ofono",
                          signal_name = "IncomingMessage",
                          path_keyword="path",
                          interface_keyword="interface")

  bus.add_signal_receiver(sms_outgoing_message_added,
                          bus_name="org.ofono",
                          signal_name = "MessageAdded")

  bus.add_signal_receiver(sms_outgoing_message_removed,
                          bus_name="org.ofono",
                          signal_name = "MessageRemoved")

  bus.add_signal_receiver(sms_property_changed,
                          bus_name="org.ofono",
                          signal_name = "PropertyChanged",
                          interface_keyword="interface",
                          path_keyword="path")




  mainloop = GLib.MainLoop()
  try:
    mainloop.run()
  except KeyboardInterrupt:
    print("SMS KeyboardInterrupt caught.")
  
  log("info", "[ofono.py] [sms_listener] Done")


def mms_listener(incoming_queue, status_queue):
  # https://kernel.googlesource.com/pub/scm/network/ofono/mmsd/+/master/test/monitor-mms

  log("info", "[ofono.py] [mms_listener] Starting")

  dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
  bus = dbus.SessionBus()

  def mms_incoming_message(name, value, member, path, interface):
    log("info", "[ofono.py] [mms_incoming_message] Starting")
    log("debug", "[ofono.py] [mms_incoming_message] name "+str(name))
    log("debug", "[ofono.py] [mms_incoming_message] member "+str(member))
    log("debug", "[ofono.py] [mms_incoming_message] path "+str(path))
    log("debug", "[ofono.py] [mms_incoming_message] interface "+str(interface))
    log("debug", "[ofono.py] [mms_incoming_message] value "+str(value))

    all_attachments =  []
    recipients=[]
    mms_attachment = {"id": 0,                            # row id in the message database.
                      "rooms_id": 0,                      # row id in the rooms database (what room it's in)
                      "matrix_room": "",                  # Room in the remote matrix server
                      "sender": "",                       # who sent the file to me
                      "recipients": [],                   # who the file was sent to
                      "send_via_modem": False,            # send via modem ( Matrix -> python app -> modem -> destination)
                      "sent_time": "",                    # time it was sent
                      "content_type": "",                 # mime type (text/plain, image/jpeg, etc)
                      "content": "",                      # Content (written to a file if it's not text/plain)
                      "subject": "",                      # Subject of the MMS
                      "content_size": "",                 # size (bytes) of the contect we have recieved.
                      "bridged": False,                   # has the message made it to matrix
                      "locked": True,                     # work in progress for this message (so we dont retry an in progress message when we check for failed ones)
                      "attempt": 0,                       # the number of attempts to bridge the message (so we can give up after X tries)
                      "matrix_event_id": "",              # Event ID from matrix
                      "modem_event_id": str(path),        # Event ID from the modem
                    }

    for mms_key in value:

      if (mms_key == "Status"):
        if (value[mms_key] == "draft"):
          log("debug", "[ofono.py] [mms_incoming_message] " + str(mms_key) + ": " + str(value[mms_key]) + " (we're sending something).")
          mms_status = {"modem_event_id": str(name),
                        "state" : str(value[mms_key]).lower(),
                       }

          log("debug", "[ofono.py] [mms_incoming_message] Putting on status_queue "+ str(mms_status))
          status_queue.put(copy.deepcopy(mms_status))
          return 0
        elif (value[mms_key] == "received"):
          log("debug", "[ofono.py] [mms_incoming_message] " + str(mms_key) + ": " + str(value[mms_key]))
        else:
          log("debug", "[ofono.py] [mms_incoming_message] SKIPPING " + str(mms_key) + ": " + str(value[mms_key]))
          return 0

      elif (mms_key == "Recipients"):
        for recipient in value[mms_key]:
          mms_attachment["recipients"].append(contacts.format_number(recipient))
        log("debug", "[ofono.py] [mms_incoming_message] recipients " + str(mms_attachment["recipients"]))

      elif (mms_key == "Silent"):
        log("debug", "[ofono.py] [mms_incoming_message] Discard " + str(mms_key) + ": " + str(value[mms_key]))
        log("info", "[ofono.py] [mms_incoming_message] Skipping Silent MMS message.")
        return 0

      elif (mms_key == "Attachments"):
        all_attachments = value[mms_key]

      elif (mms_key == "Smil"):
        log("debug", "[ofono.py] [mms_incoming_message] Discard " + str(mms_key) + ": " + str(value[mms_key]))

      elif (mms_key == "Date"):
        mms_attachment["sent_time"] = str(value[mms_key])
        log("debug", "[ofono.py] [mms_incoming_message] keeping " + str(mms_key) + ": " + mms_attachment["sent_time"])

      elif (mms_key == "Received"):
        log("debug", "[ofono.py] [mms_incoming_message] Discard " + str(mms_key) + ": " + str(value[mms_key]))

      elif (mms_key == "Sender"):
        mms_attachment["sender"] = contacts.format_number(value[mms_key])
        log("debug", "[ofono.py] [mms_incoming_message] keeping " + str(mms_key) + ": " + mms_attachment["sender"])

      elif (mms_key == "Subject"):
        mms_attachment["subject"] = str(value[mms_key])
        log("debug", "[ofono.py] [mms_incoming_message] keeping " + str(mms_key) + ": " + mms_attachment["subject"])

      elif (mms_key == "Error"):
        log("error", "[ofono.py] [mms_incoming_message] ERROR with incoming MMS message. Turn on debug to see message.")
        log("debug", "[ofono.py] [mms_incoming_message] ERROR with incoming MMS message: " + str(value[mms_key]))
        return 0

      else:
        log("debug", "[ofono.py] [mms_incoming_message] unknown key Discard: " + str(mms_key) + " value: " + str(value[mms_key]))


    ignore_list = conf.reader("cell", "ignore_csv").split(",")
    if mms_attachment["sender"] in ignore_list:
      log("info", "[ofono.py] [mms_incoming_message] Sender in ignore list")
      return 0

    if ( len(all_attachments) == 0):

      mms_attachment["content_type"] = "text/plain"
      mms_attachment["content"] = "ERROR FROM BOT: Received an MMS without any content. Cell Provider likely removed the attachment. Ask sender to send another way."
      log("debug", "[ofono.py] [mms_incoming_message] I get here when someone attaches a .txt file to an MMS. It's just gone. I've also seen this when my provider was having issues.")
      mms_attachment["content_size"] = len(mms_attachment["content"])
      incoming_queue.put(copy.deepcopy(mms_attachment))

    else:
      for attachment in all_attachments:
        log("debug", "[ofono.py] [mms_incoming_message] attachment: " + str(attachment))

        if ( "application/smil" in attachment[1] ):
          log("info", "[ofono.py] [mms_incoming_message] skipping smil attachment.")
          continue

        # pull out the attachment content
        with open(attachment[2], 'rb') as file:
          file.seek(attachment[3])  # Move the file pointer to the start byte position
          attachment_bytes = file.read(attachment[4])  # Read the content within the specified range

        mms_attachment["content_type"] = str(attachment[1])
        mms_attachment["content_size"] = str(attachment[4])
        log("info", "[ofono.py] [mms_incoming_message] content_type: " + mms_attachment["content_type"])

        # Write content to a file
        if ( "text/plain;charset=" in mms_attachment["content_type"] ):
          charset=mms_attachment["content_type"].split("=",1)[1]
          log("debug", "[ofono.py] [mms_incoming_message] trying to decode with charset " + charset)
          mms_attachment["content"] = str(attachment_bytes, charset)

          mms_attachment["content_type"] = "text/plain"

        elif ( "text/plain" == mms_attachment["content_type"] ):
          # iPhone does this for me.
          mms_attachment["content"] = str(attachment_bytes, "latin-1") # "Safe" default with no invalid byte sequences

        else:
          log("info", "[ofono.py] [mms_incoming_message] attachment needs to be written to a file.")
          mms_attachment["content"] = write_attachment_file(attachment_bytes, mms_attachment["content_type"], mms_attachment["sender"])


        log("debug", "[ofono.py] [mms_incoming_message] posting to message queue " + str(mms_attachment))
        incoming_queue.put(copy.deepcopy(mms_attachment))

    log("info", "[ofono.py] [mms_incoming_message] Done")


  def mms_property_changed(name, value, member, path, interface):
    if ( name == "Status" ):
      log("info", "[ofono.py] [mms_property_changed] Starting")
      log("debug", "[ofono.py] [mms_property_changed] name: " + str(name))
      log("debug", "[ofono.py] [mms_property_changed] value: " + str(value))
      log("debug", "[ofono.py] [mms_property_changed] member: " + str(member))
      log("debug", "[ofono.py] [mms_property_changed] path: " + str(path))
      log("debug", "[ofono.py] [mms_property_changed] interface: " + str(interface))

      mms_status = {"modem_event_id": str(path),
                    "state" : str(value).lower(),
                   }

      log("debug", "[ofono.py] [mms_property_changed] Putting on status_queue "+ str(mms_status))
      status_queue.put(copy.deepcopy(mms_status))

      log("info", "[ofono.py] [mms_property_changed] Done")


  bus.add_signal_receiver(mms_incoming_message,
                          bus_name="org.ofono.mms",
                          signal_name = "MessageAdded",
                          member_keyword="member",
                          path_keyword="path",
                          interface_keyword="interface")

  bus.add_signal_receiver(mms_property_changed,
                          bus_name="org.ofono.mms",
                          signal_name = "PropertyChanged",
                          member_keyword="member",
                          path_keyword="path",
                          interface_keyword="interface")



  mainloop = GLib.MainLoop()

  try:
    mainloop.run()
  except KeyboardInterrupt:
    print("MMS KeyboardInterrupt caught.")
  
  log("info", "[ofono.py] [mms_listener] Done")
