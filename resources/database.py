import resources.conf as conf
from resources.logging import log

import sqlite3
from pathlib import Path
import threading
import time
import random

db_dir = conf.reader("matrixtexting","database_dir")
Path(db_dir).mkdir(parents=True, exist_ok=True)
database_file = db_dir+"/matrixtexting.sqlite3"


lock_db = threading.Lock()

def build():
  log("info", "[database.py] [build] Starting")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    # Get a cursor object
    cursor = database_connection.cursor()
    cursor.execute('''CREATE TABLE IF NOT EXISTS messages (
                      id INTEGER PRIMARY KEY, 
                      rooms_id INTEGER,
                      sender TEXT,
                      recipients TEXT,
                      send_via_modem INTEGER,
                      sent_time TEXT,
                      content_type TEXT,
                      content TEXT,
                      subject TEXT,
                      content_size INTEGER,
                      bridged INTEGER,
                      locked INTEGER,
                      attempt INTEGER,
                      matrix_event_id TEXT,
                      modem_event_id TEXT )''')
    database_connection.commit()

    cursor.execute('''CREATE TABLE IF NOT EXISTS rooms (
                      id INTEGER PRIMARY KEY, 
                      room TEXT,
                      member TEXT )''')
    database_connection.commit()
 
    cursor.execute('''CREATE TABLE IF NOT EXISTS group_members (
                      id INTEGER PRIMARY KEY, 
                      mmsgroup TEXT,
                      number TEXT )''')
    database_connection.commit()

    cursor.execute('''CREATE TABLE IF NOT EXISTS matrix (
                      id INTEGER PRIMARY KEY, 
                      sync TEXT )''')
    database_connection.commit()
    cursor.execute('''INSERT OR IGNORE INTO matrix(ID) VALUES(1);''')
    database_connection.commit()

    database_connection.close()
  log("info", "[database.py] [build] Done")

def update_matrix_sync(sync):
  log("info", "[database.py] [update_matrix_sync] Starting")
  log("debug", "[database.py] [update_matrix_sync] new sync time: " + sync)

  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE matrix SET sync = ? WHERE id = ?"
    data = (sync, 1,)
    cursor.execute(query, data)

    database_connection.commit()
    database_connection.close()


  log("info", "[database.py] [update_matrix_sync] Done")
  return 1

def get_matrix_sync():
  log("info", "[database.py] [get_matrix_sync] Starting")

  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "SELECT sync FROM matrix WHERE id = ?"
    data = (1,)
    cursor.execute(query, data)
    database_connection.commit()
    row = cursor.fetchone()
    database_connection.close()

  next_batch = None
  if row is not None:
    next_batch = str(row[0])
    log("info", "[database.py] [get_matrix_sync] next_batch: " + next_batch)
  else:
    log("warning", "[database.py] [get_matrix_sync] Can't get sync time from DB.")

  log("info", "[database.py] [get_matrix_sync] Done")
  return next_batch

def check_for_room(matrix="", participants=None):
  if participants is None:
    participants = []
  log("info", "[database.py] [check_for_room] Starting")
  room_id = 0
  row = None

  if matrix:
    log("info", "[database.py] [check_for_room] checking for room via matrix room ID")
    log("debug", "[database.py] [check_for_room] matrix room ID: " + matrix)

    with lock_db:
      database_connection = sqlite3.connect(database_file)
      cursor = database_connection.cursor()
      try:
        cursor.execute('''SELECT id FROM rooms WHERE room=?''', (matrix,))
        row = cursor.fetchone()
        database_connection.commit()
      except Exception as problem:
        log("debug", "[database.py] [check_for_room] Problem with SQL: " +str(problem))
      finally:
        database_connection.close()
    if row is not None and row[0] > 0:
      log("info", "[database.py] [check_for_room] Found room.")
      room_id = row[0]
      log("debug", "[database.py] [check_for_room] room ID: " + str(room_id))
    else:
      log("info", "[database.py] [check_for_room] No Room Found.")




  else:
    log("info", "[database.py] [check_for_room] checking for room via cell numbers")
    log("info", "[database.py] [check_for_room] cell numbers: " +str(participants))

    if len(participants) == 0:
      log("error", "[database.py] [check_for_room] no participants???")

    elif len(participants) == 1:
      log("info", "[database.py] [check_for_room] 1:1 chat.")
      with lock_db:
        database_connection = sqlite3.connect(database_file)
        cursor = database_connection.cursor()
        try:
          cursor.execute('''SELECT id FROM rooms WHERE member=?''', (participants[0],))
          row = cursor.fetchone()
          database_connection.commit()
        except Exception as problem:
          log("debug", "[database.py] [check_for_room] Problem with SQL: " + str(problem))
        finally:
          database_connection.close()
      if row is not None and row[0] > 0:
        log("info", "[database.py] [check_for_room] Found room.")
        room_id = row[0]
        log("debug", "[database.py] [check_for_room] room ID: " + str(room_id))
      else:
        log("info", "[database.py] [check_for_room] No room.")
        room_id = 0

    else:
      log("info", "[database.py] [check_for_room] group chat.")
      with lock_db:
        database_connection = sqlite3.connect(database_file)
        cursor = database_connection.cursor()

        # Get All the groups one of the participants are in.
        all_possible_groups = []
        try:
          cursor.execute('''SELECT mmsgroup FROM group_members WHERE number=?''', (participants[0],))
          all_possible_groups = cursor.fetchall()
          database_connection.commit()
        except Exception as problem:
          log("debug", "[database.py] [check_for_room] Problem with SQL: " + str(problem))


        if len(all_possible_groups) >= 1:
          log("info", "[database.py] [check_for_room] Found Possible Group(s)")
          #for all the groups that the participant is in.
          for group in all_possible_groups:
            log("debug", "[database.py] [check_for_room] Possible Group: " + str(group[0]))
            #get all the members in each group
            try:
              cursor.execute('''SELECT number FROM group_members WHERE mmsgroup=?''', (group[0],))
              all_members_in_group_rows = cursor.fetchall()
              database_connection.commit()
            except Exception as problem:
              log("debug", "[database.py] [check_for_room] Problem with SQL: " + str(problem))

            #build a list to compare with what we were provided.
            other_group_members = []
            for row in all_members_in_group_rows:
              other_group_members.append(row[0])

            log("debug", "[database.py] [check_for_room] all other group members: " + str(other_group_members))

            #compare the two lists.
            other_group_members.sort()
            participants.sort()
            if other_group_members == participants:
              log("info", "[database.py] [check_for_room] match")
              #if we found a matching list of members, get the room ID
              try:
                cursor.execute('''SELECT id FROM rooms WHERE member=?''', (group[0],))
                cursor_room_id = cursor.fetchone()
                database_connection.commit()
              except Exception as problem:
                log("debug", "[database.py] [check_for_room] Problem with SQL: " + str(problem))

              if cursor_room_id is not None and cursor_room_id[0] > 0:
                room_id = cursor_room_id[0]
                log("debug", "[database.py] [check_for_room] ID: " + str(room_id))
                break
              else:
                log("warning", "[database.py] [check_for_room] DB Group room in a weird state. Check debug logs.")

        else:
          log("info", "[database.py] [check_for_room] No Possible Groups")

        database_connection.close()


  log("info", "[database.py] [check_for_room] Done")
  return room_id

def create_room(participants=None, matrix_room=""):
  if participants is None:
    participants = []
  log("info", "[database.py] [create_room] Starting")
  room_id=0

  if len(participants) == 1:
    log("info", "[database.py] [create_room] 1:1 chat.")

    with lock_db:
      database_connection = sqlite3.connect(database_file)
      cursor = database_connection.cursor()
      cursor.execute('''INSERT INTO rooms(room,member) VALUES(?,?)''',  (matrix_room,participants[0],) )
      database_connection.commit()
      room_id = cursor.lastrowid
      database_connection.close()

  else:
    log("info", "[database.py] [create_room] group chat.")
    group ="group:"+str(int(time.time()))+str(random.randrange(10, 99))
    with lock_db:
      database_connection = sqlite3.connect(database_file)
      cursor = database_connection.cursor()
      cursor.execute('''INSERT INTO rooms(room,member) VALUES(?,?)''',  (matrix_room,group,) )
      database_connection.commit()
      room_id = cursor.lastrowid

      for number in participants:
        cursor.execute('''INSERT INTO group_members (mmsgroup,number) VALUES(?,?)''', (group,number,))
        database_connection.commit()
      database_connection.close()

  log("debug", "[database.py] [create_room] created " + str(room_id))
  log("info", "[database.py] [create_room] Done")
  return room_id

def get_numbers_in_a_room(room_id):
  log("info", "[database.py] [get_numbers_in_a_room] Starting")

  numbers = []

  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    log("info", "[database.py] [get_numbers_in_a_room] Get All the groups one of the participants are in.")
    cursor.execute('''SELECT member FROM rooms WHERE id=?''', (room_id,))
    row = cursor.fetchone()
    database_connection.commit()
    if row is not None and row[0] is not "":
      member = row[0]
      if member.startswith("group:"):
        log("info", "[database.py] [get_numbers_in_a_room] Group/MMS room. getting numbers.")
        cursor.execute('''SELECT number FROM group_members WHERE mmsgroup=?''', (member,))
        all_numbers_rows = cursor.fetchall()
        database_connection.commit()
        if len(all_numbers_rows) >= 2:
          log("info", "[database.py] [get_numbers_in_a_room] Found Numbers.")
          for number_rows in all_numbers_rows:
            number = str(number_rows[0])
            log("debug", "[database.py] [get_numbers_in_a_room] Number: " + number)
            numbers.append(number)
        else:
          log("error", "[database.py] [get_numbers_in_a_room] This should be a group room... Check database group_members??")


      else:
        log("info", "[database.py] [get_numbers_in_a_room] One to One room.")
        numbers.append(str(member))

    else:
      log("info", "[database.py] [get_numbers_in_a_room] Cant find room ID.")
      log("debug", "[database.py] [get_numbers_in_a_room] room ID: " + str(room_id))

    database_connection.close()

  log("info", "[database.py] [get_numbers_in_a_room] Done")
  return numbers

def get_matrix_room(rooms_id):
  log("info", "[database.py] [get_matrix_room] Starting")
  log("debug", "[database.py] [get_matrix_room] rooms_id: " +str(rooms_id))

  row = None
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    cursor.execute('''SELECT room FROM rooms WHERE id=?''', (rooms_id,))
    row = cursor.fetchone()
    database_connection.commit()
    database_connection.close()

  if row is not None and row[0] is not "":
    log("info", "[database.py] [get_matrix_room] Found matrix room.")
    matrix_room = row[0]
    log("debug", "[database.py] [get_matrix_room] matrix room : " + str(matrix_room))
  else:
    log("info", "[database.py] [get_matrix_room] No matrix room")
    matrix_room = None

  log("info", "[database.py] [get_matrix_room] Done")
  return matrix_room

def write_message(message):
  log("info", "[database.py] [write_message] Starting")
  log("debug", "[database.py] [write_message] INSERT the following dict: " + str(message))

  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()
#    cursor.execute('''INSERT INTO messages(rooms_id,sender,recipients,send_via_modem,sent_time,content_type,content,subject,content_size,bridged,locked,attempt,matrix_event_id,modem_event_id)
#                   VALUES(?,?,?,?,?,?,?,?,?,?,?,?,?,? )''', 
#                   (message["rooms_id"], message["sender"], str(message["recipients"]), message["send_via_modem"], message["sent_time"], message["content_type"], message["content"], message["subject"], message["content_size"], message["bridged"], message["locked"], message["attempt"], message["matrix_event_id"], message["modem_event_id"]))
    query = '''
      INSERT INTO messages (
        rooms_id, sender, recipients, 
        send_via_modem, sent_time, content_type, 
        content, subject, content_size, 
        bridged, locked, attempt, 
        matrix_event_id, modem_event_id
      )
      VALUES (?, ?, ?, 
              ?, ?, ?, 
              ?, ?, ?, 
              ?, ?, ?,
              ?, ?
      )
    '''

    values = (
      message["rooms_id"], message["sender"], ','.join(message["recipients"]),
      message["send_via_modem"], message["sent_time"], message["content_type"],
      message["content"], message["subject"], message["content_size"],
      message["bridged"], message["locked"], message["attempt"],
      message["matrix_event_id"], message["modem_event_id"]
    )

    cursor.execute(query, values)




    database_connection.commit()
    row_id = cursor.lastrowid
    database_connection.close()

  log("debug", "[database.py] [write_message] database ID: " + str(row_id))
  log("info", "[database.py] [write_message] Done")
  return row_id

def update_attempt(message_id, attempt):
  log("info", "[database.py] [update_attempt] Called")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE messages SET attempt = ? WHERE id = ?"
    data = (attempt, message_id)
    cursor.execute(query, data)

    database_connection.commit()
    database_connection.close()

  log("debug", "[database.py] [update_attempt] updated ID: " + str(message_id) + " with attempt " + str(attempt))
  log("info", "[database.py] [update_attempt] Done")
  return message_id

def unlock_message(message_id):
  log("info", "[database.py] [unlock_message] Called")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE messages SET locked = ? WHERE id = ?"
    data = (False, message_id)
    cursor.execute(query, data)

    database_connection.commit()
    database_connection.close()

  log("debug", "[database.py] [unlock_message] unlocked message ID: " + str(message_id))
  log("info", "[database.py] [unlock_message] Done")
  return message_id

def lock_message(message_id):
  log("info", "[database.py] [lock_message] Called")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE messages SET locked = ? WHERE id = ?"
    data = (True, message_id)
    cursor.execute(query, data)

    database_connection.commit()
    database_connection.close()

  log("debug", "[database.py] [lock_message] locked message ID: " + str(message_id))
  log("info", "[database.py] [lock_message] Done")
  return message_id

def update_room_id(message_id, message_room_id):
  log("info", "[database.py] [update_room_id] Starting")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE messages SET rooms_id = ? WHERE id = ?"
    data = (message_room_id, message_id)
    cursor.execute(query, data)

    database_connection.commit()
    database_connection.close()

  log("debug", "[database.py] [update_room_id] message ID: " + str(message_id) + " in room: " + str(message_room_id) )
  log("info", "[database.py] [update_room_id] Done")
  return True

def update_matrix_event_id(message_id, matrix_event_id):
  log("info", "[database.py] [update_matrix_event_id] Starting")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE messages SET matrix_event_id = ? WHERE id = ?"
    data = (matrix_event_id, message_id)
    cursor.execute(query, data)

    database_connection.commit()
    database_connection.close()

  log("debug", "[database.py] [update_matrix_event_id] message ID: " + str(message_id) + " matrix event ID: " + str(matrix_event_id) )
  log("info", "[database.py] [update_matrix_event_id] Done")
  return message_id

def update_modem_event_id(message_id, modem_event_id):
  log("info", "[database.py] [update_modem_event_id] Starting")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE messages SET modem_event_id = ? WHERE id = ?"
    data = (modem_event_id, message_id)
    cursor.execute(query, data)

    database_connection.commit()
    database_connection.close()

  log("debug", "[database.py] [update_modem_event_id] message ID: " + str(message_id) + " modem event ID: " + str(modem_event_id) )
  log("info", "[database.py] [update_modem_event_id] Done")
  return message_id

def mark_bridged(message_id):
  log("info", "[database.py] [mark_bridged] Starting")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE messages SET bridged = ? WHERE id = ?"
    data = (True, message_id)
    cursor.execute(query, data)

    database_connection.commit()
    database_connection.close()

  log("debug", "[database.py] [mark_bridged] message ID: " + str(message_id) + " marked as bridged" )
  log("info", "[database.py] [mark_bridged] Done")
  return message_id

def update_content(message_id, content):
  log("info", "[database.py] [update_content] Called")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE messages SET content = ? WHERE id = ?"
    data = (content, message_id)
    cursor.execute(query, data)

    database_connection.commit()
    database_connection.close()

  log("debug", "[database.py] [update_content] updated ID: " + str(message_id) + " with content " + str(content))
  log("info", "[database.py] [update_content] Done")
  return message_id

def update_recipients(message_id, recipients):
  log("info", "[database.py] [update_recipients] Starting")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE messages SET recipients = ? WHERE id = ?"
    data = (','.join(recipients), message_id)
    cursor.execute(query, data)

    database_connection.commit()
    database_connection.close()

  log("debug", "[database.py] [update_recipients] message ID: " + str(message_id) + " with recipients: " + str(recipients) )
  log("info", "[database.py] [update_recipients] Done")
  return True

def update_message_bridged_status(modem_event_id, status):
  log("info", "[database.py] [update_message_bridged_status] Starting")
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    query = "UPDATE messages SET bridged = ? WHERE modem_event_id = ?"
    data = (status, modem_event_id)
    cursor.execute(query, data)
    #TODO test if this actually worked.

    database_connection.commit()
    database_connection.close()

  log("debug", "[database.py] [update_message_bridged_status] modem_event ID: " + str(modem_event_id) + " marked as: " + str(status) )
  log("info", "[database.py] [update_message_bridged_status] Done")
  return status

def get_matrix_room_with_event_id(matrix_event_id="", modem_event_id=""):
  log("info", "[database.py] [get_matrix_room_with_event_id] Starting")
  matrix_room = None

  row = None
  matrix_room = None
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    if matrix_event_id:
      cursor.execute('''SELECT rooms_id FROM messages WHERE matrix_event_id=?''', (matrix_event_id,))
    else:
      cursor.execute('''SELECT rooms_id FROM messages WHERE modem_event_id=?''', (modem_event_id,))

    row = cursor.fetchone()
    database_connection.commit()
    database_connection.close()

  if row is not None and row[0] is not "":
    log("info", "[database.py] [get_matrix_room_with_event_id] Found rooms id.")
    rooms_id = str(row[0])
    log("debug", "[database.py] [get_matrix_room_with_event_id] rooms_id : " + rooms_id)
    matrix_room = get_matrix_room(rooms_id)
  else:
    log("info", "[database.py] [get_matrix_room_with_event_id] Cant find rooms_id.")

  log("debug", "[database.py] [get_matrix_room_with_event_id] matrix_room : " + str(matrix_room))
  log("info", "[database.py] [get_matrix_room_with_event_id] Done")
  return matrix_room

def get_matrix_event_with_modem_event(modem_event_id):
  log("info", "[database.py] [get_matrix_event_with_modem_event] Starting")

  row = None
  matrix_event_id = None
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()
    cursor.execute('''SELECT matrix_event_id FROM messages WHERE modem_event_id=?''', (modem_event_id,))
    row = cursor.fetchone()
    database_connection.commit()
    database_connection.close()

  if row is not None and row[0] is not "":
    log("info", "[database.py] [get_matrix_event_with_modem_event] Found matrix_event_id.")
    matrix_event_id = str(row[0])
  else:
    log("info", "[database.py] [get_matrix_event_with_modem_event] Cant find rooms_id.")

  log("debug", "[database.py] [get_matrix_event_with_modem_event] matrix_room : " + str(matrix_event_id))
  log("info", "[database.py] [get_matrix_event_with_modem_event] Done")
  return matrix_event_id

def leave_room(matrix_room):
  log("info", "[database.py] [leave_room] Starting")
  log("debug", "[database.py] [leave_room] Matrix room " + matrix_room)


  deleted = False
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()
    cursor.execute('''select id, member from rooms where room=?;''', (matrix_room,))
    row = cursor.fetchone()
    if row is not None:
      log("debug", "[database.py] [leave_room] deleting everything for rooms_id " + str(row[0]))
      cursor.execute('''DELETE FROM rooms WHERE room=? ''', (matrix_room,))
      cursor.execute('''DELETE FROM messages WHERE rooms_id=? ''', (row[0],))
      cursor.execute('''DELETE FROM group_members WHERE mmsgroup=? ''', (row[1],))
      deleted = True
    else:
      log("info", "[database.py] [leave_room] Matrix room does not match any local rooms")

    database_connection.commit()
    database_connection.close()


  log("info", "[database.py] [leave_room] Done")
  return deleted

def get_failed_message(attempt_limit):
  log("info", "[database.py] [get_failed_messages] Starting")

  message = None
  with lock_db:
    database_connection = sqlite3.connect(database_file)
    cursor = database_connection.cursor()

    cursor.execute(
      '''SELECT id,rooms_id,sender,recipients,send_via_modem,sent_time,content_type,content,subject,content_size,bridged,locked,attempt,matrix_event_id,modem_event_id FROM messages WHERE locked=0 and attempt<?;''',
      (attempt_limit,))
    row = cursor.fetchone()
    database_connection.commit()
    database_connection.close()

  if row is not None and row[0] is not "":
    log("info", "[database.py] [get_failed_messages] Found failed message.")

    # get matrix room.
    matrix_room = get_matrix_room(row[1])
    log("debug", "[database.py] [get_failed_messages] matrix room: " +str(matrix_room))

    message = {"id": row[0],                    # row id in the message database.
               "rooms_id": row[1],              # row id in the rooms database (what room it's in)
               "matrix_room": matrix_room,      # Room in the remote matrix server
               "sender": row[2],                # who sent the file to me
               "recipients": row[3].split(","), # LIST of who got this message.
               "send_via_modem": row[4],        # send via modem ( Matrix -> python app -> modem -> destination)
               "sent_time": row[5],             # time it was sent
               "content_type": row[6],          # mime type (text/plain, image/jpeg, etc)
               "content": row[7],               # Content of the attahment in byte form
               "subject": row[8],               # Subject of the MMS
               "content_size": row[9],          # size (bytes) of the contect we have recieved.
               "bridged": row[10],              # has the message made it to matrix
               "locked": row[11],               # work in progress for this message (so we dont retry an in progress message when we check for failed ones)
               "attempt": row[12],              # the number of attempts to bridge the message (so we can give up after X tries)
               "matrix_event_id": row[13],      # Event ID from matrix
               "modem_event_id": row[14],       # Event ID from the modem
              }
    log("debug", "[database.py] [get_failed_messages] message: " + str(message))
  else:
    log("info", "[database.py] [get_failed_messages] No failed message.")


  log("info", "[database.py] [get_failed_messages] Done")
  return message

