#!/home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/python3

#for dbus listening - sms/mms recieve.
from gi.repository import GLib
import dbus
import dbus.mainloop.glib
import sys


def send_sms_message(phone_number, message ):
  bus = dbus.SystemBus()
  manager = dbus.Interface(bus.get_object('org.ofono', '/'),
                                        'org.ofono.Manager')
  modems = manager.GetModems()
  path = modems[0][0]
  mm = dbus.Interface(bus.get_object('org.ofono', path),
                                        'org.ofono.MessageManager')

  mm.SetProperty("UseDeliveryReports", dbus.Boolean(1))

  try:
    path = mm.SendMessage(phone_number, message)
    message = ""
    error = False
  except Exception as problem: 
    path = ""
    message = str(problem)
    error = True

  return {"error": error, "path": path, "message": message}


def main():
  if len(sys.argv) != 3:
    print("usage:")
    print("    " + sys.argv[0] + " \"+19995551234\" \"test sms message\"")

  else:
    sms = send_sms_message(sys.argv[1], sys.argv[2])
    if sms['error']:
      print(sms['message'])
    else:
      print(sms['path'])

if __name__ == "__main__":
  main()
