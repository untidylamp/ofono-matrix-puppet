#!/home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/python3

# dbus-monitor "interface=org.ofono.mms.Service"

import sys
import dbus
from pathlib import Path
import time
import random
import csv
import magic

def return_new_empty_mms_path():
  # Try to build a unqie mms file for each message, just in case. Return a default if there's a problem.  
  outbound_mms_file="/tmp/file.mms"

  try:
    outbound_dir="/tmp/"
    Path(outbound_dir).mkdir(parents=True, exist_ok=True)
    outbound_mms_file=outbound_dir+str(int(time.time()))+"-"+str(random.randrange(10, 99))+".mms"
  except Exception as problem:
    print("Problem building unique file: " + problem)

  return outbound_mms_file


def send_mms_message(phone_numbers, text, mms_file_path):

  #Phone numbers needs to be type list.
  phone_numbers = phone_numbers.split(',')
  print("MMS to: "+str(phone_numbers))

  #get the path to our mms modem.
  bus = dbus.SessionBus()
  services = bus.call_blocking("org.ofono.mms", "/org/ofono/mms",
                               "org.ofono.mms.Manager",
                               "GetServices",
                               None, [], timeout=5)
  path = services[0][0]


  #Build a DBUS Array that we're going to add our attachments to.
  attachments = dbus.Array([],signature=dbus.Signature('(sss)'))


  # text needs to be in an .mms file
  if ( text != "" ):
    # Make sure dir exists and get a unique filename to use user that dir.
    text_mms_file=return_new_empty_mms_path()

    # Write our text to that .mms file
    try:
      with open(text_mms_file, 'w') as f: 
        f.write(text)
      print("MMS message: "+text)

    except Exception as problem:
      print("Failed to write MMS text file: %s", problem)
      return None

    # build and append the string to our attachment list
    attachments.append(dbus.Struct((dbus.String("text.txt"),dbus.String("text/plain"),dbus.String(text_mms_file)),signature=None))


  if (mms_file_path != ""):
    # Build and append the file attachment to our attachment list.
    file_mime=magic.from_file(mms_file_path, mime=True)
    attachments.append(dbus.Struct((dbus.String("file.jpg"),dbus.String(file_mime),dbus.String(mms_file_path)),signature=None))
    print("MMS file type: "+file_mime)
    print("MMS file path: "+mms_file_path)

  try:
    return_path = bus.call_blocking("org.ofono.mms", path,
                             "org.ofono.mms.Service",
                             'SendMessage',
                              None, (phone_numbers, attachments), timeout=5)


    
    print("MMS on modem: "+return_path)

  except Exception as problem: 
    print("MMS Failed to send. %s", problem)
    return_path = "error"
 
#  if ( text != None ):
#    os_remove_file(outbound_txt_mms)
#  if (mms_file_path != None):
#    os_remove_file(mms_file_path)



 
  return return_path


def main():
  if len(sys.argv) != 4:
    print("usage:")
    print("    " + sys.argv[0] + " \"+19995551234\" \"test sms message\" \"/path/to/file.jpg\"")
    print("    " + sys.argv[0] + " \"+19995551234,+12225559876\" \"test sms message\" \"/path/to/file.jpg\"")
    print("    " + sys.argv[0] + " \"+19995551234,+12225559876\" \"\" \"/path/to/file.jpg\"")
    print("    " + sys.argv[0] + " \"+19995551234,+12225559876\" \"test sms message\" \"\"")


  else:
    mms = send_mms_message(sys.argv[1], sys.argv[2], sys.argv[3])


if __name__ == "__main__":
  main()