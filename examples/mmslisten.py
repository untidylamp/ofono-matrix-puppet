#!/home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/python3

#for dbus listening - sms/mms recieve.
from gi.repository import GLib
import dbus
import dbus.mainloop.glib
import sys

def incoming_mms_message(name, value, member, path, interface):
  print("-------------------------------------")
  print("incoming_mms_message")
  print(name)

  for mms_key in value:
    print(mms_key)

    if (mms_key == "Recipients"):
      for recipient in value[mms_key]:
        print(recipient)

    elif (mms_key == "Attachments"):
      for attachment in value[mms_key]:
        print(attachment[0]) # maybe filename?
        print(attachment[1]) # attachment type image/gif text/plain;charset=utf-8 image/jpeg application/smil
        print(attachment[2]) # location of .mms file
        print(attachment[3]) # number of header bytes in mms file
        print(attachment[4]) # number of data bytes in mms file.
        # I open .mms file, read the number of header (smil?) bytes, discard them, read the number of data bytes, write data bytes to a file. This file is now of type attachment[1]

    else:
      print(value[mms_key])


  print(member)
  print(path)
  print(interface)
  print("-------------------------------------")

def mms_property_changed(name, value, member, path, interface):
  print("-------------------------------------")
  print("mms_property_changed")
  print(name)
  print(value)
  print(member)
  print(path)
  print(interface)
  print("-------------------------------------")

def main():
  #https://kernel.googlesource.com/pub/scm/network/ofono/mmsd/+/master/test/monitor-mms

  dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
  bus = dbus.SessionBus()
  bus.add_signal_receiver(incoming_mms_message,
                          bus_name="org.ofono.mms",
                          signal_name = "MessageAdded",
                            member_keyword="member",
                            path_keyword="path",
                            interface_keyword="interface")

  bus.add_signal_receiver(mms_property_changed,
                          bus_name="org.ofono.mms",
                          signal_name = "PropertyChanged",
                            member_keyword="member",
                            path_keyword="path",
                            interface_keyword="interface")



  mainloop = GLib.MainLoop()
  mainloop.run()


if __name__ == "__main__":
  main()
