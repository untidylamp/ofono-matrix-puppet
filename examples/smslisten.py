#!/home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/python3

#for dbus listening - sms/mms recieve.
from gi.repository import GLib
import dbus
import dbus.mainloop.glib
import sys

def incoming_sms_message(message, details, path, interface):
  print("-------------------------------------")
  print("incoming_sms_message")
  print(message)
  print(details)
  print(path)
  print(interface)
  print("-------------------------------------")

def outgoing_sms_message_added(property, value):
  print("-------------------------------------")
  print("outgoing_sms_message_added")
  print(property)
  print(value)
  print("-------------------------------------")


def outgoing_sms_message_removed(property):
  print("-------------------------------------")
  print("outgoing_sms_message_removed")
  print(property)
  print("-------------------------------------")


def main():
  dbus.mainloop.glib.DBusGMainLoop(set_as_default=True)
  bus = dbus.SystemBus()
  bus.add_signal_receiver(incoming_sms_message,
                          bus_name="org.ofono",
                          signal_name = "ImmediateMessage",
                          path_keyword="path",
                          interface_keyword="interface")

  bus.add_signal_receiver(incoming_sms_message,
                          bus_name="org.ofono",
                          signal_name = "IncomingMessage",
                          path_keyword="path",
                          interface_keyword="interface")

  #ofono confirm SMS was sent.
  bus.add_signal_receiver(outgoing_sms_message_added,
                          bus_name="org.ofono",
                          signal_name = "MessageAdded")

  bus.add_signal_receiver(outgoing_sms_message_removed,
                          bus_name="org.ofono",
                          signal_name = "MessageRemoved")



                          
  mainloop = GLib.MainLoop()
  mainloop.run()


if __name__ == "__main__":
  main()
