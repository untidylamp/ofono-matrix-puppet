#!/home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/python3
import time

import resources.ofono as modem # can we change this to modem manager???
import resources.database as db
from resources.logging import log
import resources.matrix as matrix
import resources.conf as conf
import resources.contacts as contacts


import sys
import threading
import queue
from pathlib import Path
import copy


lock_bridge_to_matrix = threading.Lock()

def check_for_failed(stop_event, outgoing_queue, incoming_queue):
  log("info", "[app.py] [check_for_failed] Starting")
  sleep_between_retry_failed = int(conf.reader("matrixtexting", "sleep_between_retry_failed"))
  failed_retry_limit = conf.reader("matrixtexting", "retry_limit")

  while not stop_event.is_set():
    for i in range(sleep_between_retry_failed):
      if stop_event.is_set(): #TODO is this the best way???
        return 0
      time.sleep(1)

    log("info", "[app.py] [check_for_failed] Looking for Failed messages in the Database.")
    failed_message = db.get_failed_message(failed_retry_limit)

    if not failed_message:
      log("info", "[app.py] [check_for_failed] No failed messages.")
      continue

    log("info", "[app.py] [check_for_failed] Found at failed message.")
    log("debug", "[app.py] [check_for_failed] message: " + str(failed_message))

    db.lock_message(failed_message['id'])
    if failed_message['send_via_modem']:
      outgoing_queue.put(copy.deepcopy(failed_message))
    else:
      incoming_queue.put(copy.deepcopy(failed_message))

  log("info", "[app.py] [check_for_failed] Done")

def message_status(status_queue):
  log("info", "[app.py] [message_status] Starting")

  for item in iter(status_queue.get, None):
    log("debug", "[app.py] [message_status] item pulled from the queue: " + str(item))

    if "matrix_room" in item and item['matrix_room']:
      log("info", "[app.py] [message_status] Update from Matrix.")
      if item['state'] == "leave":
        log("info", "[app.py] [message_status] someone left a room. Leaving.")
        db.leave_room(item['matrix_room'])
        # TODO do we want to leave the room if any ONE of the trusted users leave???
        # TODO delete failed attachments that have not been bridged yet.... (linux file path in content. Content type != plain/text)
        try:
          matrix.leave_room(item['matrix_room'])
        except Exception as problem:
          log("error", "[app.py] [message_status] cant leave room. bot stuck in here forever (not a BIG deal). just annoying.")
          log("debug","[app.py] [message_status] error leaving room: " + str(problem))
      elif item['state'] == "room_name":
        log("info", "[app.py] [message_status] New room name. setting my display name.")
        matrix.set_display_name(item['matrix_room'], item['name'])
      elif item['state'] == "invite":
        log("info", "[app.py] [message_status] Invite to a new room.")
        joined = matrix.join_room(item['matrix_room'])
        if not joined:
          log("warning", "[app.py] [message_status] Cant join the room that I've been invited to.")
          continue
        topic = matrix.get_topic(item['matrix_room'])
        if not topic:
          log("warning", "[app.py] [message_status] Can't get topic.")
          matrix.send_emote(item['matrix_room'], "cant get topic. Try again and invite me again.")
          matrix.leave_room(item['matrix_room'])
          continue
        else:
          try:
            topic = topic.replace(" ", "")
            numbers = topic.split(",")
          except Exception as problem:
            log("warning", "[app.py] [message_status] Problem splitting topic into a list of numbers.")
            log("debug", "[app.py] [message_status] Problem with topic: " + str(problem))
            matrix.send_emote(item['matrix_room'], "I don't like the topic. " + str(problem))
            matrix.leave_room(item['matrix_room'])
            continue

        formatted_list_of_numbers = []
        for number in numbers:
          formatted_list_of_numbers.append(contacts.format_number(number))
        log("debug", "[app.py] [message_status] using numbers: " + str(formatted_list_of_numbers))
        matrix.send_emote(item['matrix_room'], "using numbers: " +  str(formatted_list_of_numbers))

        room_id = db.check_for_room(participants=formatted_list_of_numbers)
        if room_id:
          log("info", "[app.py] [message_status] Found duplicate room.")
          matrix.send_emote(item['matrix_room'], "Room already in database as ID " + str(room_id))
          matrix.leave_room(item['matrix_room'])
          continue

        room_id = db.create_room(participants=formatted_list_of_numbers, matrix_room=item['matrix_room'])
        if not room_id:
          log("warning", "[app.py] [message_status] Failed to make room in local database.")
          matrix.send_emote(item['matrix_room'], "Failed to make room in Database. Check logs. ")
          matrix.leave_room(item['matrix_room'])
          continue

        log("debug", "[app.py] [message_status] new room DB ID: " + str(room_id))
        matrix.send_emote(item['matrix_room'], "new room DB ID: " + str(room_id))

      else:
        log("warning", "[app.py] [message_status] received Matrix status change not supported.")
        log("debug", "[app.py] [message_status] Not supported: " +str(item))

    elif "modem_event_id" in item and item['modem_event_id']:
      log("info", "[app.py] [message_status] Update from modem.")
      # SMS['state'] - pending -> sent -> removed
      # MMS['state'] - draft   -> sent

      if item['state'] == "pending" or item['state'] == "draft":
        log("info", "[app.py] [message_status] Sleeping to allow bridge code to update database.")
        time.sleep(2)
        log("debug", "[app.py] [message_status] message " + str(item['modem_event_id']) + " added to modem.")
        db.update_message_bridged_status(item['modem_event_id'], 2)
      elif item['state'] == "transienterror" or item['state'] == "permanenterror":
        log("warning", "[app.py] [message_status] Problem Sending message! likely the cell provider")
        db.update_message_bridged_status(item['modem_event_id'], 5)
        matrix_room = db.get_matrix_room_with_event_id(modem_event_id=item['modem_event_id'])
        matrix.send_emote(matrix_room, "ERROR sending message: " + str(item['state']))
        # db.unlock_message(message_id) # TODO unlock the message to get a retry
      elif item['state'] == "sent":
        log("debug", "[app.py] [message_status] message " + str(item['modem_event_id']) + " sent via modem.")
        db.update_message_bridged_status(item['modem_event_id'], 3)
        matrix_room = db.get_matrix_room_with_event_id(modem_event_id=item['modem_event_id'])
        matrix_event = db.get_matrix_event_with_modem_event(modem_event_id=item['modem_event_id'])
        if matrix_room and matrix_event:
          log("info", "[app.py] [message_status] marking room read up to what the modem has sent.")
          log("debug", "[app.py] [message_status] matrix_room " + matrix_room + " sent/read up to: " + matrix_event)
          matrix.read_receipt(matrix_room, matrix_event)
        else:
          log("warning", "[app.py] [message_status] Issue marking room read. Message likely sent from another source.")
          log("debug", "[app.py] [message_status] matrix_room " + str(matrix_room) + " sent/read up to: " + str(matrix_event))


      elif item['state'] == "removed":
        log("debug", "[app.py] [message_status] message " + str(item['modem_event_id']) + " removed from modem.")
        db.update_message_bridged_status(item['modem_event_id'], 4)

      else:
        log("warning", "[app.py] [message_status] unknown state from modem.")
        log("debug", "[app.py] [message_status] unknown state: " + item['state'])
        matrix_room = db.get_matrix_room_with_event_id(modem_event_id=item['modem_event_id'])
        matrix.send_emote(matrix_room, "unknown message state: " + str(item['state']))
        # db.unlock_message(message_id) # TODO unlock the message to get a retry


    log("info", "[app.py] [message_status] Status Done")
    status_queue.task_done()
  log("info", "[app.py] [message_status] Done")
  return 0

def bridge_to_modem(outgoing_queue):
  log("info", "[app.py] [bridge_to_modem] Starting")

  for message in iter(outgoing_queue.get, None):
    log("debug", "[app.py] [bridge_to_modem] item pulled from the queue: " + str(message))

    if message['id'] == 0:
      log("info", "[app.py] [bridge_to_modem] Writing new message to database")
      message['id'] = db.write_message(message)
      if message['id'] == 0:
        log("error", "[app.py] [bridge_to_modem] Cant write message to database. Data lost.")
        # TODO - what should I do here?
        continue
      else:
        log("debug", "[app.py] [bridge_to_modem] message ID in Database " + str(message['id']))

    else:
      log("info", "[app.py] [bridge_to_modem] already in the Database (retrying failed) attempt += 1 and try to bridge again.")
      message['attempt'] += 1
      log("debug", "[app.py] [bridge_to_modem] attempt " + str(message['attempt']))
      db.update_attempt(message['id'], message['attempt'])


    message['rooms_id'] = db.check_for_room(matrix=message['matrix_room'])
    if message['rooms_id'] == 0:
      log("error", "[app.py] [bridge_to_modem] Matrix Room NOT in database. We should never get here.")
      log("debug", "[app.py] [bridge_to_modem] room not in DB: " + str(message['matrix_room']))
      matrix.send_emote(message['matrix_room'], "This room is not in Bot Database. Check bot logs or make a new room.")
      db.unlock_message(message['id'])
      continue

    log("info", "[app.py] [bridge_to_modem] update message in db with it's room.")
    update_room_id = db.update_room_id(message['id'], message['rooms_id'])
    if update_room_id:
      log("info", "[app.py] [bridge_to_modem] message updated with room ID")
    else:
      log("error", "[app.py] [bridge_to_modem] Can not update message with room ID.")
      db.unlock_message(message['id'])
      continue

    log("info", "[app.py] [bridge_to_modem] trying to Get recipients to send the message to via the modem.")
    message['recipients'] = db.get_numbers_in_a_room(message['rooms_id'])
    if len(message['recipients']) > 0:
      log("info", "[app.py] [bridge_to_modem] Got recipients from database.")
      log("debug", "[app.py] [bridge_to_modem] recipients: " + str(message['recipients']))
    else:
      log("error", "[app.py] [bridge_to_modem] Failed to get recipients from database.")
      db.unlock_message(message['id'])
      continue

    log("info", "[app.py] [bridge_to_modem] trying to Update Database With recipients")
    update_recipients = db.update_recipients(message['id'], message['recipients'])
    if update_recipients:
      log("info", "[app.py] [bridge_to_modem] Updated Database With recipients")
    else:
      log("error", "[app.py] [bridge_to_modem] Failed to update database with recipients")
      db.unlock_message(message['id'])
      continue

    log("info", "[app.py] [bridge_to_modem] check for mxc url and convert it.")
    if message['content_type'] != "text/plain" and message['content'].startswith("mxc:"):
      url = matrix.mxc_to_url(message['content'])
      if url.startswith("http"):
        log("info", "[app.py] [bridge_to_modem] Converted mxc to URL.")
        db.update_content(message['id'],url)
      else:
        log("warning", "[app.py] [bridge_to_modem] Cant convert mxc to url. Try again later")
        db.unlock_message(message['id'])
        continue

    log("info", "[app.py] [bridge_to_modem] Check if the file size is under what the user set and if the type is in the allow list.")
    allowed_mms_mime_list = conf.reader("matrixtexting", "mms_allowed_mimes_csv").split(",")
    max_mms_size_bytes = int(conf.reader("matrixtexting", "max_mms_size_bytes"))
    max_sms_char_size = int(conf.reader("matrixtexting", "max_sms_char_size"))

    local_file_path = None
    if message['content_type'] != "text/plain" and message['content_type'] in allowed_mms_mime_list and message['content_size'] < max_mms_size_bytes:
      log("info", "[app.py] [bridge_to_modem] File should be downloaded and sent as an MMS.")
      local_file_path = matrix.download_file(message['content'])
      if local_file_path:
        log("info", "[app.py] [bridge_to_modem] Attachment downloaded.")
        log("debug", "[app.py] [bridge_to_modem] Local Path: " + local_file_path)
        message['content'] = local_file_path
      else:
        log("warning", "[app.py] [bridge_to_modem] Failed to download file. Sending it as link.")
        message['content_type'] = "text/plain"

    elif message['content_type'] != "text/plain":
      log("info","[app.py] [bridge_to_modem] File is either too big or not in allow list. Sending as URL. Meaning the Content(URL) is now text/plain.")
      log("debug","[app.py] [bridge_to_modem] File size: " + str(message['content_size']))
      log("debug","[app.py] [bridge_to_modem] File mime: " + str(message['content_type']))
      message['content_type'] = "text/plain"

    if len(message['recipients']) == 1 and message['content_type'] == "text/plain" and (max_sms_char_size == 0 or message['content_size'] < max_sms_char_size):
      log("info", "[app.py] [bridge_to_modem] Plain text to one person. SMS.")
      message['modem_event_id'] = modem.send_sms_message(message['recipients'][0], message['content'])
    else:
      log("info", "[app.py] [bridge_to_modem] Sending MMS.")
      if message['content_type'] == "text/plain":
        log("info", "[app.py] [bridge_to_modem] MMS plain text message.")
        try:
          message['modem_event_id'] = modem.send_mms_message(phone_numbers=message['recipients'],text=message['content'])
        except Exception as problem:
          log("error", "[app.py] [bridge_to_modem] Error sending mms.")
          log("debug", "[app.py] [bridge_to_modem] Error sending mms " +str(problem))
      else:
        log("info", "[app.py] [bridge_to_modem] MMS attachment")
        try:
          message['modem_event_id'] = modem.send_mms_message(phone_numbers=message['recipients'],text="",mms_file_path=message['content'],mms_file_mime=message['content_type'])
        except Exception as problem:
          log("error", "[app.py] [bridge_to_modem] Error sending mms.")
          log("debug", "[app.py] [bridge_to_modem] Error sending mms " +str(problem))


    if local_file_path:
      log("info", "[app.py] [bridge_to_modem] Delete downloaded attachment.")
      try: Path(local_file_path).unlink()
      except: pass

    if not message['modem_event_id']:
      log("error", "[app.py] [bridge_to_modem] Failed to put message on modem for sending.")
      db.unlock_message(message['id'])
      continue

    log("info", "[app.py] [bridge_to_modem] Message on Modem. Updating database.")
    log("debug", "[app.py] [bridge_to_modem] Modem event id: " + message['modem_event_id'])

    log("info", "[app.py] [bridge_to_modem] update message database with matrix_event_id")
    db.update_modem_event_id(message['id'], message['modem_event_id'])

    log("info", "[app.py] [bridge_to_modem] mark message as bridged")
    db.mark_bridged(message['id'])

    log("info", "[app.py] [bridge_to_modem] Message done")
    # if we spam ofono too quickly it will fail to send the message.
    time.sleep(1)
    outgoing_queue.task_done()

  log("info", "[app.py] [bridge_to_modem] Done")
  return 0

def bridge_to_matrix(incoming_queue):
  log("info", "[app.py] [bridge_to_matrix] Starting")

  for message in iter(incoming_queue.get, None):
    log("debug", "[app.py] [bridge_to_matrix] item pulled from the queue: " + str(message))

    #If the message is not in the database, write it to the database. If the message is in the database attempt += 1
    if(message['id'] == 0):
      log("info", "[app.py] [bridge_to_matrix] Writing message to database")
      message['id'] = db.write_message(message)
      if(message['id'] == 0):
        log("error", "[app.py] [bridge_to_matrix] Cant write message to database. Data lost.")
        # TODO - what should I do here?
        continue
      else:
        log("debug", "[app.py] [bridge_to_matrix] message in Database " + str(message['id']))

    else:
      log("info", "[app.py] [bridge_to_matrix] message in db ")
      message['attempt'] += 1
      log("debug", "[app.py] [bridge_to_matrix] attempt " + str(message['attempt']))
      db.update_attempt(message['id'], message['attempt'])

    # create a list of numbers without this cell number
    participants = message['recipients'] + [message['sender']]
    participants.remove(conf.reader("cell", "number"))

    # Check for a room. If there isn't one, create a Matrix room and put it in the database.
    message['rooms_id'] = db.check_for_room(participants=participants)
    if(message['rooms_id'] == 0):
      log("info", "[app.py] [bridge_to_matrix] Room not found.")

      matrix_room = matrix.build_room(participants)
      if matrix_room is None:
        log("error", "[app.py] [bridge_to_matrix] failed to create matrix room.")
        db.unlock_message(message['id'])
        continue
      log("debug", "[app.py] [bridge_to_matrix] matrix room created: " + str(matrix_room))

      message['rooms_id'] = db.create_room(participants, matrix_room)
      if( message['rooms_id'] == 0 ):
        log("error", "[app.py] [bridge_to_matrix] Cant create room in database.")
        db.unlock_message(message['id'])
        try:
          log("info", "[app.py] [bridge_to_matrix] Leaving room.")
          matrix.leave_room(matrix_room)
        except Exception as problem:
          log("error", "[app.py] [bridge_to_matrix] cant leave room. bot stuck in here forever (not a BIG deal). just annoying.")
          log("debug","[app.py] [bridge_to_matrix] error leaving room: " + str(problem))

        continue
      log("debug", "[app.py] [bridge_to_matrix] matrix room added to local DB room " + str(message['rooms_id']))
      display_name = ', '.join(contacts.get_name(number) for number in participants)
      matrix.set_display_name(matrix_room, display_name) # TODO contact names
      matrix.change_permissions_in_room(matrix_room)

    else:
      log("info", "[app.py] [bridge_to_matrix] local Room ID found.")
      log("debug", "[app.py] [bridge_to_matrix] Room id: " + str(message['rooms_id']))
      matrix_room = db.get_matrix_room(message['rooms_id'])
      if matrix_room is None:
        log("error", "[app.py] [bridge_to_matrix] There's a room in the database without a matrix room.")
        db.unlock_message(message['id'])
        continue

      log("debug", "[app.py] [bridge_to_matrix] matrix room: " + matrix_room)

    # update message in db with it's room.
    db.update_room_id(message['id'], message['rooms_id'])

    if "text/plain" in message['content_type']:
      matrix.send_text(matrix_room, message) #updates message['matrix_event_id']
    else:
      file_path=message['content']
      matrix.send_file(matrix_room, message) # updates message['matrix_event_id'] and message['content']
      http_url = message['content']
      if http_url.startswith("http"):
        log("debug", "[app.py] [bridge_to_matrix] telling DB the file is now at: " + http_url)
        db.update_content(message['id'], http_url)
        Path(file_path).unlink()
        log("debug", "[app.py] [bridge_to_matrix] deleted file: " + file_path)


    if message['matrix_event_id'] is None or message['matrix_event_id'] == "":
      log("error", "[app.py] [bridge_to_matrix] Failed to bridge message to matrix.")
      db.unlock_message(message['id'])
      continue

    log("debug", "[app.py] [bridge_to_matrix] Bridged to matrix: " + message['matrix_event_id'])

    log("info", "[app.py] [bridge_to_matrix] update message database with matrix_event_id")
    db.update_matrix_event_id(message['id'], message['matrix_event_id'])

    log("info", "[app.py] [bridge_to_matrix] mark message as bridged")
    db.mark_bridged(message['id'])

    log("info", "[app.py] [bridge_to_matrix] Message done")
    incoming_queue.task_done()

  log("info", "[app.py] [bridge_to_matrix] Done")

def main():
  print("Starting.")
  log("info", "[app.py] [main] Starting.")

  log("info", "[app.py] [main] Build local database")
  db.build()

  log("info", "[app.py] [main] Matrix login")
  if matrix.login() is None:
    print("Matrix Login Failed. Check debug logs.")
    sys.exit(101)

  incoming_message_queue = queue.Queue()
  message_status_queue = queue.Queue()
  outgoing_message_queue = queue.Queue()

  thread_list = []

  modem_sms_thread = threading.Thread(target=modem.sms_listener, args=(incoming_message_queue,message_status_queue,))
  modem_sms_thread.start()
  thread_list.append(modem_sms_thread)

  modem_mms_thread = threading.Thread(target=modem.mms_listener, args=(incoming_message_queue,message_status_queue,))
  modem_mms_thread.start()
  thread_list.append(modem_mms_thread)

  stop_matrix_sync_thread = threading.Event()
  matrix_sync_thread = threading.Thread(target=matrix.sync, args=(stop_matrix_sync_thread,outgoing_message_queue,message_status_queue,))
  matrix_sync_thread.start()
  thread_list.append(matrix_sync_thread)

  bridge_to_matrix_thread = threading.Thread(target=bridge_to_matrix, args=(incoming_message_queue,))
  bridge_to_matrix_thread.start()
  thread_list.append(bridge_to_matrix_thread)

  bridge_to_modem_thread = threading.Thread(target=bridge_to_modem, args=(outgoing_message_queue,))
  bridge_to_modem_thread.start()
  thread_list.append(bridge_to_modem_thread)

  message_status_thread = threading.Thread(target=message_status, args=(message_status_queue,))
  message_status_thread.start()
  thread_list.append(message_status_thread)

  stop_check_for_failed_thread = threading.Event()
  check_for_failed_thread = threading.Thread(target=check_for_failed, args=(stop_check_for_failed_thread,outgoing_message_queue,incoming_message_queue))
  check_for_failed_thread.start()
  thread_list.append(check_for_failed_thread)

  try:
    # TODO - Is this the right way to do it????
    modem_sms_thread.join()
    print("SMS thread closed")

    modem_mms_thread.join()
    print("MMS thread closed")

    # set stop so they do not enter the for loop again.
    stop_check_for_failed_thread.set()
    stop_matrix_sync_thread.set()

    print("matrix sync thread closing....")
    matrix_sync_thread.join()
    print("matrix sync thread closed")

    # Add none to the end of the queue and wait for them to finish.
    message_status_queue.put(None)
    incoming_message_queue.put(None)
    outgoing_message_queue.put(None)

    message_status_thread.join()
    print("status thread closed")

    bridge_to_matrix_thread.join()
    print("to matrix thread closed")

    bridge_to_modem_thread.join()
    print("to modem thread closed")

    print("check failed messages closing....")
    check_for_failed_thread.join()
    print("check failed messages thread closed")


    #todo kill outbound queue

  except KeyboardInterrupt:
    print("Program interrupted. TODO Cleaning up...what now?")
    sys.exit(200)
    
  print("Program exiting.")

if __name__ == "__main__":
    main()
