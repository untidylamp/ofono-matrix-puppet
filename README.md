# MatrixTexting

Python script used to bridge Matrix and SMS/MMS messages via [ofono](https://01.org/ofono). This is 
 a rewrite that probably has more bugs then the last version. If you'd like to use
 the old version go [to tag v1](https://gitlab.com/untidylamp/ofono-matrix-puppet/-/tags/V1)

My goal is to run it on a Nexus5 with [Ubuntu touch](https://devices.ubuntu-touch.io/device/hammerhead) without much customization. 
 Meaning, I decided to stick with their default Python 3.5.2 (that's why I'm still 
 using matrix_client and not matrix-nio). Encrypted rooms are not a concern for me, 
 so I didn't added support. However, PR, Issue, feature requests, etc are welcome. 
 If it works in python 3.5.2 I'll merge it. I have no formal Dev training so my code 
 will be ugly but it should work.

## Status
**Cell -> Matrix**
* [x] SMS
* [x] MMS
* [x] Group Messages


**Matrix m.room.message -> Cell**
* [x] m.text - SMS for normal chat. MMS for group chats.
* [x] m.**** - If small enough for an MMS and in the allow list, attach to MMS. Otherwise, send URL to file.


**General**
* [x] Initiate a new conversation in Matrix
* [x] Leave room when user leaves and clean up local data.
* [x] Clean up bridged MMS files.
* [x] Retry failed messages to Matrix
* [x] Retry failed messages to ofono
* [x] support [alphanumeric "numbers"](https://gitlab.com/untidylamp/mmmpuppet/-/issues/12), shortcodes, and [fake numbers](https://gitlab.com/untidylamp/mmmpuppet/-/issues/9)
* [x] Ignore incoming numbers
* [x] MMS Attachment allow list (URL sent if not in allow list)
* [X] Optional [SMS size limit](https://gitlab.com/untidylamp/mmmpuppet/-/issues/5) (send as MMS if it's too large)
* [x] Read Contacts (from vcf file)
* [x] message status from modem reflected in the local DB
* [x] move matrix read receipt AFTER Modem sends message.


## TODOs:
* testing/fixing bugs
* https://github.com/matrix-org/matrix-python-sdk/pull/303/commits/aa4071835a79b5a3dccfd366b01bd9d53ed0a62f
* any TODOs I left in the file and didn't clean up.

# Requirements

- Modem compatible with [ofono](https://01.org/ofono)
- python >= 3.5.0
- python3-pip
- pip3 install phonenumbers
- pip3 install matrix_client *PATCH REQUIRED
- TODO there's more I'm sure

***NOTE:** if you don't apply [this](https://github.com/matrix-org/matrix-python-sdk/pull/303/commits/aa4071835a79b5a3dccfd366b01bd9d53ed0a62f) patch you'll have a bad time.

## TL;DR

0.  Setup your own homeserver.. or use matrix.org via element - https://app.element.io/
1.  If you don't have an account, Create one.
2.  Create a bot account.
3.  Grab the latest version of this script.
4.  Update the settings.
5.  run and test the app
6.  submit issues or merge requests to fix any problems.


# Config Options
The default config file path can be changed in resources/conf.py -> `filepath="/home/phablet/.config/matrixtexting.ini"`

Example config provided under examples/matrixtexting.ini


#### Cell
- `number =` the cell phone number of the modem.
- `country_code =` Default country code for phonenumbers.parse(number, country_code)
- `attachments_dir =` Dir where incoming attachments will live until they're bridged.
- `ignore_csv =` a list of numbers (or whatever "sent" the message to the modem) to ignore. Check the debug logs for exact matching if it's not working.

#### matrix
- `url =` Homeserver URL https://matrix-client.matrix.org
- `bot =` This is the bot account. @smsbot:matrix.org
- `pass =` Password to login and grab a token. This is removed after we have a token and is optional if you want to supply a token manually.
- `sync_timeout_ms =` This needs to be be less then the [matrix_client patch](https://github.com/matrix-org/matrix-python-sdk/pull/303/commits/aa4071835a79b5a3dccfd366b01bd9d53ed0a62f). How long to wait for new messages from the matrix server before sleeping.
- `sleep_between_sync =` So we don't dos the server if there is a problem, wait X seconds between syncs.
- `trusted_senders_csv =` CSV list (with out spaces) on who to trust to bridge SMS/MMS messages. They'll be invited to rooms created by the bot.
- `token =` Token for the bot to sync to the server. Provide manually or set the pass to get the bot to generate one.

#### matrixtexting
- `database_dir =` Dir where the database will be stored.
- `tmp_outbound_attachment_dir =` Outbound MMS messages NEED to be written to a file then sent. This is where it will do that.
- `sleep_between_retry_failed =` Check for failed messages in the database to be bridged again in X seconds.
- `retry_limit =` How many times do you want to retry to bridge a message before the app gives up and leaves it in the database.
- `max_sms_char_size =` 0 to disable. If you have a problem with long SMSes, set this to the max working char cound. The rest will be sent as MMSes.
- `max_mms_size_bytes =` Max file size that your provider will allow in an MMS. The rest will be sent as a link. My provider allows 2MiB so I set it to 2000000 bytes to be safe.
- `mms_allowed_mimes_csv =` Some providers (mine) remove attachments if they don't like it. This is mime types allowed to be attached to an MMS file if the size is small enough. The rest will be sent as a url.
- `contacts_vcf =` I grabbed my vcf file from nextcloud and added support for that.

#### log
- `level =` debug, info, warning, error, critical. Debug has $vars (passwords, phone numbers, tokens, message content). info is what I use to comment my code. The rest are what they are without vars. I'll ALWAYS use debug to print a $var.
- `dir =` Log file location
- `bytes =` How many bytes you will allow your logfile to grow. IE 10MiB = 10485760
- `history =` number of history files.

## Usage:
All you need is python3, a Linux box, and a modem that works with [ofono](https://01.org/ofono)

Clone or download this repo as a zip. open up resources/conf.py and change line 2
 to the location you want the config file to be. IE: `filepath="/etc/matrixtexting.ini`.
 Move the example file there, change the settings you want. The password will be replaced
 with a token after the first run of the script.

To see if the script is working just tail the logs and watch it for a few seconds. 
 you should see that it logged in, got a token, and is syncing every X seconds.
 Nothing should print to the terminal, everything should be in the logs.
 Once the script is running everything should now be bridged into matrix.
 
I've tried to keep all personal information (Phone numbers, message content) in
 the debug log messages. Info is more just comments for the code and for me to
 follow exactly what it's doing.

The goal is to force all new numbers though the matrix.format_number(number)
 function so they will look the same for matching... If the script can not format
 the number to a national equivalent (IE: Canada +12505551234) it uses
 whatever the modem or user via matrix provides. This is in an attempt to support
 shortcodes and fake non-numeric numbers.


CSV lists in the config can not contain any spaces


If the bot needs to login and get a token, It will do an initial sync and discard
 everything to start "fresh". Please use a new account that only the bot uses.

**Starting a Conversation from Matrix**

*  **Room Name** - Name of contact.
*  **Room Topic** - Phone number of contact. If a group a csv list of numbers.

Create a room, Set the room name to the name of the person you wish to contact,
 and the topic to their phone number. Once you have a room with this information
 invite the bot account. It will join the room and use the topic as the numbers
 to talk to. It will accept invalid numbers, strings, etc by design.
 
If the bot finds a problem it will tell you about it and leave the room. Fix the
 problem and reinvite the bot. You can not create duplicate rooms.
 
Once you have a good room the bot will change their display name to the room
 name and update the database matching the room ID (!TdGYixONCkUyUOsUnf:matrix.org)
 to the cellphone number. Any messages posted to this room will then be send to
 the cellphone via ofono. Any reply from them will be posted in the same room.

If you change the room name, the bot will update their display name. If you
 change the topic nothing will happen. The phone number is matched in the
 database with the roomID so you probably want to make a new room.
 
**Contacts**

I grabbed my vcf file from nextcloud and added support for that.

`curl -o /home/phablet/matrix-ofono-puppet/contacts.vcf -u untidylamp "https://nextcloud.untidylamp.ca/remote.php/dav/addressbooks/users/untidylamp/contacts/?export"`



**Receiving a message for a new room**

When you receive a new message the bot will check the local database for a room.
 If there is no room the bot will create a room, update the Room Name and Topic
 to the phone number, change their display name to the phone number, update the 
 power levels so you cant change the topic and invite the user(s). If you
 change the room name the bot will update their display name.
 
If there is a room already, the bot will simply post the message to that room.

Any replies posted to the room the bot will send to the cellphone number.


**Pictures/MMS/Files/ETC**

Any file uploaded to a room will be checked for 2 things. The mime type is in
 the allow list and it's small enough to be sent as an MMS. If both are true 
 the file will be downloaded, attached to an MMS and sent. If the file is not
 in the allow list or is to large, a URL will be sent instead.

**MMSes size.** From my experience Android will  compress pictures to about 300K but I am
 able to get away with ~2MiB. This  will depend on your provider so you may need to test.

**MMSes types.** My provider started removing PDFs and other small files that I use to
 be able to send. For this reason I've added an allow list. Anything not in this list
 will be sent as a URL.

Group messages are always sent as MMSes.


**Leaving a room**

When you leave a room the bot will delete everything related to that room then 
 it will also leave the room itself. This means it will create a new room if 
 it needs to.
 
 
 **General**
 
You can view the *source of a message* in element. This will give you information 
  like the ID in the sqlite3 database, what attempt it was on to bridge the
  message, and what time it was sent.

The *read receipts* will move within matrix/element once ofono responds with "sent"
 on the dbus. In my experience when the read receipts do not move, this means the
 message never made it to the other end. Rebooting the phone has always fixed
 the issue. I've never looked into it further.
 
The *online presence* of the bot should work as expected.

The *topic* is used for the phone number you're in contact with. If you
 change the topic it will do nothing. The room is matched by the room ID and 
 the phone number listed in the database. If you make a mistake its best to
 leave the room and make a new one.
 


# Migration from V1

First, Make a backup of your current database and copy it to the new location.

The Sync time has been moved to 'matrix' table and the token is in your .ini settings file.

```
phablet@ubuntu-phablet:~/data_MatrixTexting$ sqlite3 matrixtexting.sqlite3
SQLite version 3.11.0 2016-02-15 17:29:24              
Enter ".help" for usage hints.
sqlite> .tables                                                                                                        
group_members   matrix_account  messages        rooms    
sqlite> .header on     
sqlite> select * from matrix_account; 
id|token|sync
1|syt_bbbbbbbbb|aaaaaaaaaaaaaaaaa
sqlite> DROP TABLE IF EXISTS matrix_account;                                                                            
sqlite> DROP TABLE IF EXISTS messages;                                                                                  
sqlite> INSERT INTO matrix(sync) VALUES("aaaaaaaaaaaaaaaaa");
sqlite> select * from matrix;
id|sync
1|aaaaaaaaaaaaaaaaa
sqlite> 
```

and the token
```
phablet@ubuntu-phablet:~$ grep -E "matrix|token" ~/.config/matrixtexting.ini 
[matrix]
token = syt_bbbbbbbbb
```


# Fresh Nexus4 or Nexus5 setup.

The below as been tested on both a Nexus4 and a Nexus5. I'm using a 5 daily and call forwaring my calls to a voip number.

Micro USB device that [provides power and ethernet](https://www.amazon.ca/gp/product/B01LXP5TXI) out of the box to the Nexus 5 is the "Amazon Ethernet Adapter for Amazon Fire TV Devices". $19.99 CAD currently.  I was using a script to reconnect to wifi on boot because my Nexus 5 forgot my wifi network every time but I've since started using the adapter.

### Unlock Nexus5 bootloader

[TODO](https://xdaforums.com/t/guide-nexus-5-how-to-unlock-bootloader-install-custom-recovery-and-root.2507905/)

I've already done this so I'm skipping this section for now. Link above might help.

### Install ubuntu touch to the phone

TODO - I already had this installed.... Follow the steps. 

https://devices.ubuntu-touch.io/installer/

I've installed 16.04/stable. Wipe Userdata, Bootstrap, and Format system partitions are all checked.

### Phone setup

* English (Canada)
* Connect to wifi
* Set the city
* Skip name
* Create passcode (numbers only)
* 1471

okay, next, exit, swipe, etc the rest away

**Open termainal (on the phone)**

    mkdir -pm700 ~/.ssh
    ip a
    nc -l 1234 > ~/.ssh/authorized_keys

**Note down the ip address** of the phone from `ip a` and go to your main PC.

    nc 192.168.x.y 1234 < ~/.ssh/ed25519.public

I had to ctrl+c on my laptop to exit the prompt. The phone returned to the prompt also.

**Back on the phone**

    sudo su -
    1471
    service ssh start

**On main PC**

    ssh phablet@192.168.x.y


The following is done on the phone via SSH. It can be typed out on the phone manually if SSH isn't working but it's a lot. it also takes awhile.

**As phablet on phone via ssh or termainal:**

    libertine-container-manager create --id ofono-matrix-puppet
    libertine-container-manager exec -i ofono-matrix-puppet -c "/bin/bash"
    python3 --version
    apt install -y python3-pip curl screen python3-chardet python3-magic
    pip3 install certifi==2017.4.17 requests==2.22 phonenumbers matrix_client
    exit ####out of libertine
    mkdir ~/.local/bin
    cd ~/.local/bin/
    ln -s /home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/python3
    ln -s /home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/pip3
    ln -s /home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/curl
    ln -s /home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/screen
    cd ~

**fix vim**

    cat > ~/.vimrc << EOF
    set nocompatible
    set nu
    set backspace=indent,eol,start
    EOF

**Fix matrix_client**

I don't know a better way to do this. -> https://github.com/matrix-org/matrix-python-sdk/pull/303/commits/aa4071835a79b5a3dccfd366b01bd9d53ed0a62f

    cd ~/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/local/lib/python3.5/dist-packages/matrix_client/
    vi api.py
    vi client.pi
    cd ~

**Download Contacts from NextCloud via cli (optional)**

_Note:_ You can just download from the web UI if the below doesn't work for some reason. make sure to replace my information with yours.

    mkdir -p /home/phablet/data_MatrixTexting/
    curl -o /home/phablet/data_MatrixTexting/contacts.vcf -u untidylamp "https://nextcloud.untidylamp.ca/remote.php/dav/addressbooks/users/untidylamp/contacts/?export"


**Download this repo**

    cd ~
    wget https://gitlab.com/untidylamp/ofono-matrix-puppet/-/archive/master/ofono-matrix-puppet-master.zip
    unzip ofono-matrix-puppet-master.zip

**edit the config**

Update where you want the config file (line 2). Default of `filepath="/home/phablet/.config/matrixtexting.ini"` will be used in the rest of this readme

    vi ~/ofono-matrix-puppet-master/resources/conf.py


Copy example conf and update it as needed.

    cp ~/ofono-matrix-puppet-master/examples/matrixtexting.ini /home/phablet/.config/matrixtexting.ini
    vi /home/phablet/.config/matrixtexting.ini

From the default example file I changed:

* [cell].number
* [matrix].url
* [matrix].bot
* [matrix].pass
* [matrix].trusted_senders_csv


**Start script to make sure config is correct**

    /home/phablet/.local/bin/python3 /home/phablet/ofono-matrix-puppet-master/main.py


It should show `Starting.` and then look like it's hung. Wait couple seconds then Ctrl+C and wait for it to stop.

Once it returns to the prompt, it's a good idea to check the config file to make sure the password has been removed and it has a token.

    less /home/phablet/.config/matrixtexting.ini

You can also check the log files to see if there was any errors.

    less ~/data_MatrixTexting/logs/matrixtexting.log


I use a cron job to start this on boot in screen as noted below.

**Reboot nightly and run custom script on boot.**

Reboot nightly because ubuntu touch seems to hold on to MMS file handels and insead of trying to fix it I reboot nightly.

    sudo su -
    1471
    mount -o remount,rw /
    crontab -e

add the following two lines

    00 3 * * * /sbin/reboot
    @reboot /home/phablet/boot.sh

and remount as ro.

    mount -o remount,ro /

### Boot script

In here you can do whatever you want. I did it this way so I dont have to edit the read only system partition too much.

    touch /home/phablet/boot.sh
    chmod 755 /home/phablet/boot.sh
    vi /home/phablet/boot.sh


Below is some of the code from my boot script. There might be some lines around that are left over from testing 

**Start SSH on boot**

This should be after the network is up :) 

    su - root -c "/etc/init.d/ssh restart"

**Start this python script on boot in screen**


    date  &>>  /home/phablet/boot.log
    mkdir -v /var/run/screen  &>>  /home/phablet/boot.log
    chmod -v 777 /var/run/screen  &>>  /home/phablet/boot.log
    su - phablet -c "/home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/screen -dm script -c /home/phablet/ofono-matrix-puppet-master/main.py -fa /home/phablet/data_MatrixTexting/logs/std.log"
    su - phablet -c "/home/phablet/.cache/libertine-container/ofono-matrix-puppet/rootfs/usr/bin/screen -ls" &>>  /home/phablet/boot.log
    date &>>  /home/phablet/boot.log


Logs for screen output is `/home/phablet/data_MatrixTexting/logs/std.log`. 


**Connect to wifi network**

    nmcli connection delete "WiFiNameHere" ; nmcli connection delete "WiFiNameHere 1" ; nmcli connection delete "WiFiNameHere 2"
    nmcli dev wifi connect "WiFiNameHere" password "WiFiPasswordHere" &>>  /home/phablet/boot.log
    sed '/^mac-address=/d' -i /etc/NetworkManager/system-connections/WiFiNameHere  &>>  /home/phablet/boot.log
    nmcli con mod "WiFiNameHere" ipv4.address 192.168.x.y/24 &>>  /home/phablet/boot.log
    nmcli con mod "WiFiNameHere" ipv4.gateway 192.168.x.1 &>>  /home/phablet/boot.log
    nmcli con mod "WiFiNameHere" ipv4.dns 192.168.x.1 &>>  /home/phablet/boot.log
    nmcli con mod "WiFiNameHere" ipv4.method static &>>  /home/phablet/boot.log
    nmcli con down "WiFiNameHere" &>>  /home/phablet/boot.log
    nmcli con up "WiFiNameHere" &>>  /home/phablet/boot.log
